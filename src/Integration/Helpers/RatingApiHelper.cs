﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Integration.RatingApi;

namespace Integration.Helpers
{
    public class RatingApiHelper
    {
        public List<VSDFBRes> DoWork(string awb)
        {
            var api = new MobileAppWebServiceSoapClient();
            var result = api.ViewShipmentFB(awb, "vSdFb$ervice").ToList();
            return result;
        }
    }
}
