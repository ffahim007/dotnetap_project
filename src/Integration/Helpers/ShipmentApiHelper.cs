﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Core.Infrastructure;
using Newtonsoft.Json.Linq;
using Common.Helper;
using StackExchange.Redis;
using Core.DTO;

namespace Integration.Helpers
{
    public class TokenObject
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }

    public class ShipmentApiHelper
    {
        RedisConnection obj = new RedisConnection();

        private async Task<string> GetToken()
        {
            string token = "";
            //Creating Redis database instance  
            IDatabase db = obj.Connection.GetDatabase();

            //Getting value of "shipmentApiAuthToken" key  
            token = db.StringGet("shipmentApiAuthToken");

            //If key not found in Redis database  
            if (token == null)
            {
                var authApi = await HttpRequestHelper.Execute("POST", "http://smsacloud.azurewebsites.net/api/token", JSONHelper.ConvertObjectToJSON(new TokenObject { UserName = "uroojzahid", Password = "KQSFC6-P" }));
                if (authApi.IsSuccessStatusCode)
                {
                    var authApiResult = await authApi.Content.ReadAsStringAsync();
                    var authApiObject = JObject.Parse(authApiResult);
                    token = authApiObject["token"].ToString();
                }

                //Setting value for key with Expiry time of 24 Hours  
                db.StringSet("shipmentApiAuthToken", token, TimeSpan.FromHours(23));
            }

            return token;
        }

        public async Task<ShipmentDetailsDTO> DoWork(string awb)
        {
            string token = await GetToken();
            var shipmentApi = await HttpRequestHelper.Execute("GET", "http://smsacloud.azurewebsites.net/api/Shipment/" + awb, JSONHelper.ConvertObjectToJSON(new object()), token);
            if (shipmentApi.IsSuccessStatusCode)
            {
                var shipmentApiResult = await shipmentApi.Content.ReadAsStringAsync();
                var shipmentDetails = new ShipmentDetailsDTO(shipmentApiResult);
                var shipperAddressApi = await HttpRequestHelper.Execute("GET", "http://smsacloud.azurewebsites.net/api/Address/" + shipmentDetails.OriginCountry + "/" + shipmentDetails.ShipperPhone, JSONHelper.ConvertObjectToJSON(new object()), token);
                if (shipperAddressApi.IsSuccessStatusCode)
                {
                    var shipperAddressApiResult = await shipperAddressApi.Content.ReadAsStringAsync();
                    var shipperAddress = JObject.Parse(shipperAddressApiResult);

                    shipmentDetails.ShipperAddress1 = string.IsNullOrEmpty(shipperAddress["address"].ToString()) ? shipmentDetails.ShipperAddress1 : shipperAddress["address"].ToString();
                    shipmentDetails.ShipperAddress2 = string.IsNullOrEmpty(shipperAddress["address"].ToString()) ? shipmentDetails.ShipperAddress2 : string.Empty;
                    shipmentDetails.ShipperLocation = string.IsNullOrEmpty(shipperAddress["latLng"].ToString()) ? shipmentDetails.ShipperLocation : shipperAddress["latLng"].ToString();
                }

                var receiverAddressApi = await HttpRequestHelper.Execute("GET", "http://smsacloud.azurewebsites.net/api/Address/" + shipmentDetails.DestCountry + "/" + shipmentDetails.ReceiverPhone, JSONHelper.ConvertObjectToJSON(new object()), token);
                if (receiverAddressApi.IsSuccessStatusCode)
                {
                    var receiverAddressApiResult = await receiverAddressApi.Content.ReadAsStringAsync();
                    var receiverAddress = JObject.Parse(receiverAddressApiResult);

                    shipmentDetails.ReceiverAddress1 = string.IsNullOrEmpty(receiverAddress["address"].ToString()) ? shipmentDetails.ReceiverAddress1 : receiverAddress["address"].ToString();
                    shipmentDetails.ReceiverAddress2 = string.IsNullOrEmpty(receiverAddress["address"].ToString()) ? shipmentDetails.ReceiverAddress2 : string.Empty;
                    shipmentDetails.ReceiverLocation = string.IsNullOrEmpty(receiverAddress["latLng"].ToString()) ? shipmentDetails.ReceiverLocation : receiverAddress["latLng"].ToString();
                }

                return shipmentDetails;
            }

            return null;
        }
    }
}
