﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using Common.Helper;
using Core.Constant;
using Core.DTO;
using Core.Entity;
using Core.Enum;
using Core.Helper;
using Core.Infrastructure;
using Core.IRepository;
using Core.IService;
using Microsoft.AspNet.Identity;
using Recipe.Common.Helper;
using Recipe.Core.Base.Generic;

namespace Service
{
    public class UserService : Service<IUserRepository, ApplicationUser, UserDTO, string>, IUserService
    {
        private ISystemUnitOfWork unitOfWork;
        private IExceptionHelper exceptionHelper;
        private ISystemRequestInfo requestInfo;
        private ApplicationUserManager manager;
        private IPermissionService permissionService;
        private IRoleService roleService;
        private IUserRepository userRepository;
        private IConfigurationService configurationService;

        public UserService(
            ISystemUnitOfWork unitOfWork,
            IExceptionHelper exceptionHelper,
            ISystemRequestInfo requestInfo,
            IPermissionService permissionService,
            IRoleService roleService,
            IUserRepository userRepository,
            IConfigurationService configurationService)
            : base(unitOfWork, unitOfWork.UserRepository)
        {
            this.unitOfWork = unitOfWork;
            this.exceptionHelper = exceptionHelper;
            this.requestInfo = requestInfo;
            this.permissionService = permissionService;
            this.roleService = roleService;
            this.userRepository = userRepository;
            this.configurationService = configurationService;
            this.manager = new ApplicationUserManager(new ApplicationUserStore(this.requestInfo));
        }

        public async Task<int> ChangeOnline(bool isOnline, string userId = null)
        {
            userId = string.IsNullOrEmpty(userId) ? this.requestInfo.UserId : userId;

            return 0;
        }

        public async Task<TotalResultDTO<UserDTO>> GetAllUserUpdateAsync(JsonApiRequest request, string search)
        {
            Core.Enum.UserRoles role = new Core.Enum.UserRoles();
            Enum.TryParse(this.requestInfo.Role, out role);

            if (role == UserRoles.Admin)
            {
                var users = await this.Repository.GetAllForAdmin(request, search);
                var result = UserDTO.ConvertEntityListToDTOList<UserDTO>(users.Result);
                return new TotalResultDTO<UserDTO> { Result = result, TotalRecords = users.TotalRecords };
            }

            return null;
        }

        public async Task<LoginDTO> FindByEmailOrMobileAsync(AuthDTO authDto)
        {
            var entity = await this.userRepository.FindByEmailOrMobileAsync(authDto.EmailId, authDto.PhoneNumber);
            if (entity != null)
            {
                if (authDto.Role != null && entity.AccountRole != authDto.Role)
                {
                    return new LoginDTO() { Error = Message.UserUnAuthorized };
                }

                if (entity.Status == (int)UserStatus.Inactive)
                {
                    return new LoginDTO() { Error = Message.UserInActivation };
                }

                if (entity.Status == (int)UserStatus.Freeze)
                {
                    return new LoginDTO() { Error = Message.UserFreeze };
                }

                if (entity.Status == (int)UserStatus.Blocked)
                {
                    return new LoginDTO() { Error = Message.UserBlocked };
                }

                var checkPassword = await this.userRepository.CheckPasswordAsync(entity, authDto.Password);
                if (checkPassword)
                {
                    var dto = new UserDTO();
                    dto.ConvertFromEntity(entity);
                    return new LoginDTO() { User = dto };
                }
                else
                {
                    return new LoginDTO() { Error = Message.UserInvalidUserNameOrPassword };
                }
            }

            return new LoginDTO() { Error = Message.UserAccountNotExistCreateNew };
        }

        public async Task<UserDTO> FindByUserNameAsync(string userName)
        {
            var entity = await this.Repository.FindByUserNameAsync(userName);
            if (entity == null)
            {
                return null;
            }

            var dto = new UserDTO();
            dto.ConvertFromEntity(entity);
            return dto;
        }

        public async Task<UserDTO> ValidateUserAsync(string userName, string password)
        {
            try
            {
                this.manager = new ApplicationUserManager(new ApplicationUserStore(this.requestInfo));
                var entity = await this.manager.FindAsync(userName, password);
                if (entity == null)
                {
                    return null;
                }

                UserDTO userDto = await this.GetAsync(entity.Id);
                Core.Enum.UserRoles role = Roles.GetRoleObject(userDto.RoleId);

                return userDto;
            }
            catch
            {
                return null;
            }
        }

        public async Task<UserDTO> GetInitialUserDataAsync()
        {
            var userId = this.requestInfo.UserId;
            var user = await this.GetAsync(userId);
            return user;
        }

        public async Task<AuthUserDTO> GetUserData(string userId)
        {
            var user = await this.GetAsync(userId);

            AuthUserDTO authUserDTO = new AuthUserDTO();
            authUserDTO.User = user;

            return authUserDTO;
        }

        public async Task<string> ForgotPassword(string userName)
        {
            string token = new Random().Next(1000, 9999).ToString();

            ApplicationUser user = await this.Repository.FindByUserNameAsync(userName);
            if (user != null && !user.IsDeleted)
            {
                user.ResetCode = token;
                await this.Repository.UpdateUserAsync(user);

                UserDTO dto = new UserDTO(user);

                EmailConfiguration configuration = new EmailConfiguration();
                configuration.ToAddress = userName;
                configuration.EmailSubject = "Password Reset";
                configuration.EmailBody = "Use this 4 digit Code to reset your password. " + token;

                EmailHelper.SendEmail(configuration);

                //await this.notificationService.ForgotPasswordNotificationAsync(dto, token);

                return token;
            }

            return null;
        }

        public async Task DeleteMultipleUsersAsync(List<UserIdDTO> dtoList)
        {
            await this.Repository.DeleteMultipleUsersAsync(dtoList);
        }

        public async Task UpdateUserStatusAsync(List<ChangeUserStatusDTO> dtoStatus)
        {
            await this.Repository.UpdateUserStatusAsync(dtoStatus);
        }

        public async Task<string> ForgotPassword(string passwordToken, ForgotPasswordDTO dtoObject)
        {
            ApplicationUser user = await this.GetByPasswordToken(passwordToken);
            if (user != null && !user.IsDeleted)
            {
                await this.Repository.ChangePasswordAsync(user.Id, dtoObject.NewPassword);

                user.ResetCode = string.Empty;
                await this.Repository.UpdateUserAsync(user);

                return Message.UserPasswordChangeSuccessfully;
            }

            return null;
        }

        public async Task<bool> ValidateTokenAsync(string token)
        {
            return await this.GetByPasswordToken(token) != null;
        }

        public override async Task<UserDTO> CreateAsync(UserDTO dtoObject)
        {
            dtoObject.AccountRole = Roles.GetRole(dtoObject.RoleId);
            dtoObject.Status = UserStatus.Active;
            ApplicationUser user = dtoObject.ConvertToEntity();

            user = await this.Repository.CreateAsync(user, dtoObject.Password, dtoObject.AccountRole);

            dtoObject.Id = user.Id;
            return dtoObject;
        }

        public async Task<string> GetUserRole(string userId)
        {
            return await Repository.GetUserRoleAsync(userId);
        }

        public override async Task<UserDTO> UpdateAsync(UserDTO dtoObject)
        {
            var databaseEntity = await Repository.GetAsync(dtoObject.Id);
            if (databaseEntity != null)
            {
                //if SSA Role changed, check whether it is already assigned to substation or not

                databaseEntity.Fullname = dtoObject.Fullname;
                databaseEntity.Address = dtoObject.Address;
                databaseEntity.Gender = dtoObject.Gender;
                databaseEntity.Bio = dtoObject.Bio;
                databaseEntity.SecondaryEmail = dtoObject.SecondaryEmail;
                databaseEntity.Image = dtoObject.Image;

                dtoObject.ConvertFromEntity(databaseEntity);
                databaseEntity.Roles.Clear();
                var entity = dtoObject.ConvertToEntity(databaseEntity);

                await this.Repository.UpdateUserAsync(entity);
            }
            else
            {
                this.exceptionHelper.ThrowAPIException(string.Format(Message.InvalidObject, dtoObject.UserName));
            }

            return dtoObject;
        }

        public async Task<ChangePasswordDTO> ChangePasswordAsync(ChangePasswordDTO dtoObject)
        {
            if (string.IsNullOrEmpty(dtoObject.EmailId))
            {
                dtoObject.EmailId = this.requestInfo.UserName;
            }

            return await Repository.ChangePasswordAsync(dtoObject);
        }

        public async Task<AssignPasswordDTO> AssignPasswordAsync(AssignPasswordDTO dtoObject)
        {
            if (dtoObject.Password.Length < 6)
            {
                this.exceptionHelper.ThrowAPIException(Core.Constant.Message.UserPasswordLength);
            }

            var user = await this.Repository.FindByEmailOrMobileAsync(dtoObject.EmailId, dtoObject.PhoneNumber);

            if (user == null)
            {
                this.exceptionHelper.ThrowAPIException(string.Format(Message.UserAccountNotExist));
            }

            if (!dtoObject.FromWeb)
            {
                if (user.AccountRole != dtoObject.Role.ToString())
                {
                    this.exceptionHelper.ThrowAPIException(string.Format(Message.UserAccountNotExist));
                }
            }

            return await Repository.AssignPasswordAsync(user, dtoObject);
        }

        public async Task<ChangePasswordDTO> ChangeUserPasswordAsync(ChangePasswordDTO dtoObject)
        {
            if (string.IsNullOrEmpty(dtoObject.EmailId) || string.IsNullOrEmpty(dtoObject.NewPassword) || string.IsNullOrEmpty(dtoObject.OldPassword))
            {
                this.exceptionHelper.ThrowAPIException(Message.UserInvalidUserNameOrPassword);
            }

            var user = await this.Repository.FindAsync(dtoObject.EmailId, dtoObject.OldPassword);
            if (user == null)
            {
                this.exceptionHelper.ThrowAPIException(Message.UserInvalidUserNameOrPassword);
            }

            await Repository.AssignPasswordAsync(user, new AssignPasswordDTO { Password = dtoObject.NewPassword });
            return dtoObject;
        }

        public override async Task DeleteAsync(string id)
        {
            await base.DeleteAsync(id);
        }

        public async Task UpdateUserRole(string userId, string roleId)
        {
            await this.Repository.UpdateUserRoleAsync(userId, roleId);
        }

        public async Task<bool> VerifyOneTimePinAsync(OneTimePinDTO dtoObject)
        {
            if (dtoObject == null || string.IsNullOrEmpty(dtoObject.OneTimePin))
            {
                this.exceptionHelper.ThrowAPIException(Core.Constant.Message.OneTimePinRequired);
            }

            //if (dtoObject.OneTimePin.Length > 4)
            //{
            //    this.exceptionHelper.ThrowAPIException(Core.Constant.Message.OneTimePinLength);
            //}

            var result = await this.Repository.IsValidOneTimePinAsync(dtoObject.EmailId, dtoObject.PhoneNumber, dtoObject.OneTimePin);

            if (result.IsValid)
            {
                await this.Repository.ResetOTPCountAsync(dtoObject.EmailId);
                return true;
            }
            else
            {
                if (result.User != null)
                {
                    this.exceptionHelper.ThrowAPIException(Core.Constant.Message.OneTimePinInvalid);
                }
            }

            return false;
        }

        public async Task<bool> IsLockedOut(UserDTO dtoObject)
        {
            return await this.manager.IsLockedOutAsync(dtoObject.Id);
        }

        public async Task<bool> GetLockoutEnabled(UserDTO dtoObject)
        {
            return await this.manager.GetLockoutEnabledAsync(dtoObject.Id);
        }

        public async Task<IdentityResult> AccessFailed(UserDTO dtoObject)
        {
            return await this.manager.AccessFailedAsync(dtoObject.Id);
        }

        public async Task<int> GetAccessFailedCount(UserDTO dtoObject)
        {
            return await this.manager.GetAccessFailedCountAsync(dtoObject.Id);
        }

        public async Task<bool> VerifyUserNameWithIMEIForLogin(UserNameDTO dtoObject)
        {
            var user = await this.GetUser(dtoObject);
            if (user == null)
            {
                this.exceptionHelper.ThrowAPIException(Core.Constant.Message.UserInvalidUserNameOrImeiNumber);
            }

            return true;
        }

        public UserDTO GetUser(string id)
        {
            var result = this.Repository.GetUser(id);
            var userDTO = new UserDTO();
            userDTO.ConvertFromEntityForChangeRequest(result);
            return userDTO;
        }

        public bool FindByEmail(string email, string userId = null)
        {
            var user = this.Repository.FindByEmail(email, userId);
            return user == null;
        }

        public async Task<bool> UnlockUserAsync(string userId)
        {
            return await this.Repository.UnlockUserAsync(userId);
        }

        public async Task<bool> UserNameExists(string userName)
        {
            if (!string.IsNullOrWhiteSpace(userName))
            {
                var user = await this.Repository.FindByUserNameAsync(userName);
                return user != null && !user.IsDeleted;
            }

            return false;
        }

        public async Task<bool> EmailExists(string emailId)
        {
            if (!string.IsNullOrWhiteSpace(emailId))
            {
                var user = await this.Repository.FindByEmailAsync(emailId);
                return user != null && !user.IsDeleted;
            }

            return false;
        }

        public async Task<bool> IdentificationExists(string identificationNumber, int countryId)
        {
            if (!string.IsNullOrWhiteSpace(identificationNumber) && countryId > 0)
            {
            }

            return false;
        }

        public async Task<bool> EmailOrMobileExists(string email, string mobileNumber, UserRoles userRole, bool fromWeb)
        {
            var user = await this.userRepository.GetByEmailOrMobileAsync(email, mobileNumber);
            if (user == null)
            {
                this.exceptionHelper.ThrowAPIException(Core.Constant.Message.UserAccountNotExist);
            }

            if (user.IsBlocked)
            {
                this.exceptionHelper.ThrowAPIException(Core.Constant.Message.UserBlocked);
            }

            if (user.LockoutEndDateUtc != null)
            {
                this.exceptionHelper.ThrowAPIException(Core.Constant.Message.UserAccountLocked);
            }

            if (!fromWeb)
            {
                if (user.AccountRole != userRole.ToString())
                {
                    this.exceptionHelper.ThrowAPIException(string.Format(Message.UserAccountNotExist));
                }
            }

            var otpString = new CommonHelper().GenerateOTP();

            await this.Repository.UpdateOTPAsync(user.Email, otpString);

            await this.UnitOfWork.SaveAsync();

            return true;
        }

        public async Task<List<KeyValuePair<string, object>>> GetUserByRole()
        {
            var users = await this.GetAllAsync();
            var groupingUser = users.GroupBy(x => x.RoleId, (k, v) => new KeyValuePair<string, object>(k, v)).ToList();
            var userByRole = new List<KeyValuePair<string, object>>();

            foreach (var item in groupingUser)
            {
                var role = Roles.GetRole(item.Key);
                userByRole.Add(new KeyValuePair<string, object>(role, item.Value));
            }

            return userByRole;
        }

        public bool IsBlocked()
        {
            var user = this.Repository.FindByUserId();
            return user != null && user.IsBlocked;
        }

        public async Task<UserDTO> GetFacebookAccountAsync(string accessToken)
        {
            var result = await new FacebookHelper().GetAsync<dynamic>(accessToken, "me", "fields=id,name,email,first_name,last_name,age_range,birthday,gender,locale");
            if (result == null)
            {
                this.exceptionHelper.ThrowAPIException(HttpStatusCode.ExpectationFailed, string.Format(Message.NotFound, "User"));
            }

            var account = new UserDTO
            {
                Email = result.email,
                UserName = result.username,
                Password = Validations.DefaultFacebookPassword
            };

            return account;
        }

        public async Task<List<string>> MultiImageUploadAsync(MultipartMemoryStreamProvider multipart)
        {
            List<string> result = new List<string>();

            foreach (var item in multipart.Contents)
            {
                var fileBytes = await item.ReadAsByteArrayAsync();

                var ext = Path.GetExtension(item.Headers.ContentDisposition.FileName.Replace("\"", string.Empty).Replace(@"\", string.Empty));
                var filename = DateTime.UtcNow.ToString().Replace("/", string.Empty).Replace(":", string.Empty).Replace(" ", string.Empty) + ext;
                string filePath = "/Images/" + filename;
                File.WriteAllBytes(HostingEnvironment.MapPath(filePath), fileBytes);

                result.Add(filePath);
            }

            return result;
        }

        public async Task<ApplicationUser> FindByClientId(string clientId)
        {
            ApplicationUser user = await this.Repository.FindByClientId(clientId);

            return user;
        }

        #region Private Function
        private async Task<ApplicationUser> GetByPasswordToken(string token)
        {
            ApplicationUser user = await this.Repository.FindByPasswordTokenAsync(token);

            if (user == null)
            {
                ((ISystemUnitOfWork)UnitOfWork).ExceptionHelper.ThrowAPIException(Core.Constant.Message.UserInvalidToken);
            }

            ApplicationUser entity = await this.Repository.GetAsync(user.Id);
            return entity;
        }

        private async Task<ApplicationUser> GetUser(UserNameDTO dtoObject)
        {
            if (dtoObject == null || string.IsNullOrEmpty(dtoObject.UserName) || string.IsNullOrEmpty(dtoObject.ImeiNumber))
            {
                this.exceptionHelper.ThrowAPIException(string.Format(Core.Constant.Message.ObjectRequired, "Username or Imei"));
            }

            var user = await this.Repository.FindUserNameWithIMEIAsync(dtoObject.UserName, dtoObject.ImeiNumber);

            return user;
        }
        #endregion
    }
}
