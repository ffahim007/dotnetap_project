﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Constant;
using Core.DTO;
using Core.Entity;
using Core.Enum;
using Core.Extensions;
using Core.Infrastructure;
using Core.IRepository;
using Core.IService;
using Recipe.Core.Base.Generic;

namespace Service
{
    public class PermissionService : Service<IPermissionRepository, Permission, PermissionDTO, int>, IPermissionService
    {
        private ISystemRequestInfo requestInfo;
        private IExceptionHelper exceptionHelper;

        public PermissionService(
            ISystemUnitOfWork unitOfWork,
            ISystemRequestInfo requestInfo,
            IExceptionHelper exceptionHelper)
            : base(unitOfWork, unitOfWork.PermissionRepository)
        {
            this.requestInfo = requestInfo;
            this.exceptionHelper = exceptionHelper;
        }

        public async Task<PermissionDTO> Get(string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                var permissions = await this.Repository.GetAll();
                var permission = permissions.FirstOrDefault(x => x.Name.Trim().ToLower() == name.Trim().ToLower() && x.IsDeleted == false);

                if (permission == null)
                {
                    this.exceptionHelper.ThrowAPIException(Message.InvalidPermission);
                }

                var dto = new PermissionDTO();
                dto.ConvertFromEntity(permission);

                return dto;
            }

            return null;
        }

        public async Task<List<PermissionDTO>> Get(PermissionGroup group)
        {
            var permissions = await this.Repository.GetAll();
            permissions = permissions.Where(x => x.Group == group.ToString());

            return PermissionDTO.ConvertEntityListToDTOList(permissions);
        }

        public async Task<List<PermissionDTO>> GetReportPermissions(string reportName)
        {
            var reportPermissions = new List<PermissionDTO>();

            if (!string.IsNullOrWhiteSpace(reportName))
            {
                var permissions = await this.Get(PermissionGroup.Report);
                foreach (var permission in permissions)
                {
                    var reportComparer = reportName.RemoveSpaces().Trim().ToLower();
                    var permissionComparer = permission.Name.Trim().ToLower();

                    if (permissionComparer.Contains(reportComparer))
                    {
                        reportPermissions.Add(permission);
                    }
                }
            }

            return reportPermissions;
        }
    }
}
