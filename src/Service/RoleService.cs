﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Constant;
using Core.DTO;
using Core.Entity;
using Core.Enum;
using Core.Infrastructure;
using Core.IRepository;
using Core.IService;
using Recipe.Core.Base.Generic;

namespace Service
{
    public class RoleService : Service<IRoleRepository, ApplicationRole, RoleDTO, string>, IRoleService
    {
        private ISystemRequestInfo requestInfo;
        private ApplicationRoleManager manager;

        public RoleService(
            ISystemUnitOfWork unitOfWork,
            ISystemRequestInfo requestInfo)
            : base(unitOfWork, unitOfWork.RoleRepository)
        {
            this.requestInfo = requestInfo;
            this.manager = new ApplicationRoleManager(new ApplicationRoleStore(this.requestInfo));
        }

        public override async Task<IList<RoleDTO>> GetAllAsync()
        {
            var result = await base.GetAllAsync();

            return result
                .Where(x =>
                    x.Name.Trim() != UserRoles.None.ToString())
                    .ToList();
        }

        public List<RoleDTO> GetCurrentUserRoleMapping()
        {
            var role = (UserRoles)Enum.Parse(typeof(UserRoles), this.requestInfo.Role);

            var mapping = RoleMapping.GetMapping(role);
            var dtoList = new List<RoleDTO>();

            foreach (var item in mapping.Roles)
            {
                RoleDTO dto = new RoleDTO();
                dto.ConvertFromEntity(item);
                dtoList.Add(dto);
            }

            return dtoList;
        }
    }
}
