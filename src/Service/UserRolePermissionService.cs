﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTO;
using Core.Entity;
using Core.IRepository;
using Core.IService;
using Recipe.Core.Base.Generic;

namespace Service
{
    public class UserRolePermissionService : Service<IUserRolePermissionRepository, UserRolePermission, UserRolePermissionDTO, int>, IUserRolePermissionService
    {
        private ISystemUnitOfWork unitOfWork;

        public UserRolePermissionService(ISystemUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.UserRolePermissionRepository)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool GetUserPermissionsByRole(string roleId, List<int> permissionId)
        {
            var result = this.Repository.IsUserPermitted(roleId, permissionId);
            return result;
        }

        public async Task<string> GetUserRolePermission(string roleID)
        {
            var result = await this.Repository.GetUserPermissions(roleID);
            return string.Join(",", result.ToArray());
        }

        #region Private Function

        #endregion
    }
}
