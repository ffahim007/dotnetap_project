﻿namespace ControlPanel.Core.Enums
{
    public enum EventCodes
    {
        Error = 1,
        Warning = 2,
        Log = 3
    }
}
