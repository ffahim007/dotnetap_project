﻿using System;

namespace ControlPanel.Core
{
    public interface ISystemProcessThread
    {
        string ThreadName { get; set; }
        
        int? StartRange { get; set; }
        
        int? EndRange { get; set; }
        
        DateTime? LastSuccessfullyExecuted { get; set; }
        
        int? ScheduledTime { get; set; }
        
        string Initialize();
        
        string Execute(string argument);
    }
}
