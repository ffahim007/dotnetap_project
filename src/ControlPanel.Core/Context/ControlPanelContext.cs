﻿using ControlPanel.Core.Entities;
using System.Data.Entity;
using System.Reflection;

namespace ControlPanel.Core.Context
{
    public class ControlPanelContext : DbContext
    {
        public ControlPanelContext()
            : base("ControlPanel")
        {
            //Database.SetInitializer(new DropCreateDatabaseAlways<ControlPanelContext>());
            //Database.SetInitializer<ControlPanelContext>(null);
            //Configuration.ProxyCreationEnabled = false;
            //Configuration.LazyLoadingEnabled = true;
        }

        private string DirtyColumnName(PropertyInfo property)
        {

            return property.Name.ToUpper().Substring(0, property.Name.Length);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //var userId = "dbo";

            //if (ConfigurationManager.AppSettings["dbUserId"] != null)
            //{
            //    userId = ConfigurationManager.AppSettings["dbUserId"];
            //    modelBuilder.HasDefaultSchema(userId);

            //}
            //modelBuilder.Types().Configure(c => c.ToTable(c.ClrType.Name.ToUpper(), userId));
            //modelBuilder.Properties().Configure(config => config.HasColumnName(DirtyColumnName(config.ClrPropertyInfo)));
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<DriveStatistic> DriveStatistic { get; set; }

        public DbSet<NetworkStatistic> NetworkStatistic { get; set; }

        public DbSet<PerformanceStatistic> PerformanceStatistic { get; set; }

        public DbSet<SystemEventLog> SystemEventLog { get; set; }

        public DbSet<SystemProcess> SystemProcess { get; set; }

        public DbSet<SystemProcessThread> SystemProcessThread { get; set; }
    }
}
