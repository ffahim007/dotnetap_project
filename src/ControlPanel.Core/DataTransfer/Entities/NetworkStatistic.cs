﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ControlPanel.Core.Entities
{
    [DataContract]
    [Table("CP_NETWORKSTATISTIC")]
    public partial class NetworkStatistic : IEquatable<NetworkStatistic>
    {

        [Key]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 NetworkStatisticId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(500)]
        [Required]
        public virtual System.String InterfaceName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(50)]
        public virtual System.String IpAddress { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Double TotalUsage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Double Download { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Double Upload { get; set; }

        [IgnoreDataMember]
        public virtual System.DateTime CreationDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [NotMapped]
        public virtual string CreationDateStr
        {
            get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(50)]
        public virtual System.String ServerIp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(50)]
        public virtual System.String ServerName { get; set; }


        public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }

        #region IEquatable<NetworkStatisticBase> Members

        public virtual bool Equals(NetworkStatistic other)
        {
            if (this.NetworkStatisticId == other.NetworkStatisticId && this.InterfaceName == other.InterfaceName && this.IpAddress == other.IpAddress && this.TotalUsage == other.TotalUsage && this.Download == other.Download && this.Upload == other.Upload && this.CreationDate == other.CreationDate && this.ServerIp == other.ServerIp && this.ServerName == other.ServerName)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual void CopyFrom(NetworkStatistic other)
        {
            if (other != null)
            {
                this.NetworkStatisticId = other.NetworkStatisticId;
                this.InterfaceName = other.InterfaceName;
                this.IpAddress = other.IpAddress;
                this.TotalUsage = other.TotalUsage;
                this.Download = other.Download;
                this.Upload = other.Upload;
                this.CreationDate = other.CreationDate;
                this.ServerIp = other.ServerIp;
                this.ServerName = other.ServerName;
            }


        }

        #endregion



    }


}
