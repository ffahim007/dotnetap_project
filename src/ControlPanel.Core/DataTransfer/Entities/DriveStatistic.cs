﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ControlPanel.Core.Entities
{
    [DataContract]
    [Table("CP_DRIVESTATISTIC")]
    public partial class DriveStatistic : IEquatable<DriveStatistic>
    {
        [DataMember(EmitDefaultValue = false)]
        [Key]
        public virtual System.Int32 DriveStatisticId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(50)]
        public virtual System.String IpAddress { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Double? DriveSpaceAvailable { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Double? DriveTotalSpace { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(50)]
        public virtual System.String DriveName { get; set; }

        [IgnoreDataMember]
        public virtual System.DateTime? CreationDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [NotMapped]
        public virtual string CreationDateStr
        {
            get { if (CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(50)]
        public virtual System.String MachineName { get; set; }


        public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }

        #region IEquatable<DriveStatisticBase> Members

        public virtual bool Equals(DriveStatistic other)
        {
            if (this.DriveStatisticId == other.DriveStatisticId && this.IpAddress == other.IpAddress && this.DriveSpaceAvailable == other.DriveSpaceAvailable && this.DriveTotalSpace == other.DriveTotalSpace && this.DriveName == other.DriveName && this.CreationDate == other.CreationDate && this.MachineName == other.MachineName)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual void CopyFrom(DriveStatistic other)
        {
            if (other != null)
            {
                this.DriveStatisticId = other.DriveStatisticId;
                this.IpAddress = other.IpAddress;
                this.DriveSpaceAvailable = other.DriveSpaceAvailable;
                this.DriveTotalSpace = other.DriveTotalSpace;
                this.DriveName = other.DriveName;
                this.CreationDate = other.CreationDate;
                this.MachineName = other.MachineName;
            }


        }

        #endregion



    }


}
