﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace ControlPanel.Core.Entities
{
    [DataContract]
    [Table("CP_SYSTEMPROCESSTHREAD")]
    public partial class SystemProcessThread : IEquatable<SystemProcessThread>
    {

        [DataMember(EmitDefaultValue = false)]
        [Key]
        public virtual System.Int32 SystemProcessThreadId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? TenantId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 SystemProcessId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [Required]
        [MaxLength(50)]
        public virtual System.String Name { get; set; }



        [DataMember(EmitDefaultValue = false)]
        [MaxLength(50)]
        public virtual System.String SpringEntryName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(100)]
        public virtual System.String Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Boolean Enabled { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Boolean Continuous { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 SleepTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Boolean AutoStart { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(50)]
        public virtual System.String Status { get; set; }

        [FieldNameAttribute("Message", true, false, 500)]
        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Message { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual int? ScheduledTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? StartRange { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? EndRange { get; set; }

        [IgnoreDataMember]
        public virtual System.DateTime? LastSuccessfullyExecuted { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [NotMapped]
        public virtual string LastSuccessfullyExecutedStr
        {
            get { if (LastSuccessfullyExecuted.HasValue) return LastSuccessfullyExecuted.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastSuccessfullyExecuted = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 ContinuousDelay { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Boolean IsDeleted { get; set; }

        [FieldNameAttribute("DisplayOrder", false, false, 10)]
        [DataMember(EmitDefaultValue = false)]
        [MaxLength(10)]
        [Required]
        public virtual System.String DisplayOrder { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Argument { get; set; }

        [IgnoreDataMember]
        public virtual System.DateTime? LastUpdateDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [NotMapped]
        public virtual string LastUpdateDateStr
        {
            get { if (LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Double? ExecutionTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Double? EstimatedExecutionTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Boolean? ShowUpdateInLog { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [NotMapped]
        public string LastExecutedSeconds { get; set; }


        //[DataMember(EmitDefaultValue = false)]
        [MaxLength(100)]
        //[Required]
        public virtual System.String Class { get; set; }

        //[DataMember(EmitDefaultValue = false)]
        [MaxLength(100)]
        //[Required]
        public virtual System.String Namespace { get; set; }

        // [DataMember(EmitDefaultValue = false)]
        [MaxLength(100)]
        //[Required]
        public virtual System.String Assembly { get; set; }

        public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }

        #region IEquatable<SystemProcessThreadBase> Members

        public virtual bool Equals(SystemProcessThread other)
        {
            if (this.SystemProcessThreadId == other.SystemProcessThreadId && this.TenantId == other.TenantId && this.SystemProcessId == other.SystemProcessId && this.Name == other.Name && this.SpringEntryName == other.SpringEntryName && this.Description == other.Description && this.Enabled == other.Enabled && this.Continuous == other.Continuous && this.SleepTime == other.SleepTime && this.AutoStart == other.AutoStart && this.Status == other.Status && this.Message == other.Message && this.ScheduledTime == other.ScheduledTime && this.StartRange == other.StartRange && this.EndRange == other.EndRange && this.LastSuccessfullyExecuted == other.LastSuccessfullyExecuted && this.ContinuousDelay == other.ContinuousDelay && this.IsDeleted == other.IsDeleted && this.DisplayOrder == other.DisplayOrder && this.Argument == other.Argument && this.LastUpdateDate == other.LastUpdateDate && this.ExecutionTime == other.ExecutionTime && this.EstimatedExecutionTime == other.EstimatedExecutionTime && this.ShowUpdateInLog == other.ShowUpdateInLog)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual void CopyFrom(SystemProcessThread other)
        {
            if (other != null)
            {
                this.SystemProcessThreadId = other.SystemProcessThreadId;
                this.TenantId = other.TenantId;
                this.SystemProcessId = other.SystemProcessId;
                this.Name = other.Name;
                this.SpringEntryName = other.SpringEntryName;
                this.Description = other.Description;
                this.Enabled = other.Enabled;
                this.Continuous = other.Continuous;
                this.SleepTime = other.SleepTime;
                this.AutoStart = other.AutoStart;
                this.Status = other.Status;
                this.Message = other.Message;
                this.ScheduledTime = other.ScheduledTime;
                this.StartRange = other.StartRange;
                this.EndRange = other.EndRange;
                this.LastSuccessfullyExecuted = other.LastSuccessfullyExecuted;
                this.ContinuousDelay = other.ContinuousDelay;
                this.IsDeleted = other.IsDeleted;
                this.DisplayOrder = other.DisplayOrder;
                this.Argument = other.Argument;
                this.LastUpdateDate = other.LastUpdateDate;
                this.ExecutionTime = other.ExecutionTime;
                this.EstimatedExecutionTime = other.EstimatedExecutionTime;
                this.ShowUpdateInLog = other.ShowUpdateInLog;
            }


        }

        #endregion

        public ISystemProcessThread GetThread()
        {
            Assembly[] asms = AppDomain.CurrentDomain.GetAssemblies();
            var assemblyName = this.Assembly.ToLower() + ".dll";
            var assembly = asms.Where(x => x.ManifestModule.ScopeName.ToLower() == assemblyName).FirstOrDefault();
            if (assembly == null)
            {
                var binary = File.ReadAllBytes(AppDomain.CurrentDomain.BaseDirectory + "\\" + assemblyName);
                AppDomain.CurrentDomain.Load(binary);
            }
            asms = AppDomain.CurrentDomain.GetAssemblies();
            assembly = asms.Where(x => x.ManifestModule.ScopeName.ToLower() == assemblyName).FirstOrDefault();

            //TP.CodeReview.Core.dll");
            var obj = assembly.CreateInstance(this.Namespace + "." + this.Class, true);
            if (obj is ISystemProcessThread)
            {
                return (ISystemProcessThread)obj;
            }
            else
            {
                throw new Exception("Class doesn't inherit from ISystemProcessThread");
            }
        }

    }


}
