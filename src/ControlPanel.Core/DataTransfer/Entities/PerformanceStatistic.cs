﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ControlPanel.Core.Entities
{
    [DataContract]
    [Table("CP_PERFORMANCESTATISTIC")]
    public partial class PerformanceStatistic : IEquatable<PerformanceStatistic>
    {

        [DataMember(EmitDefaultValue = false)]
        [Key]
        public virtual System.Int32 PerformanceStatisticId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Double? Cpu { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Double? Memory { get; set; }

        [IgnoreDataMember]
        public virtual System.DateTime? CreationDate { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [NotMapped]
        public virtual string CreationDateStr
        {
            get { if (CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty; }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(255)]
        public virtual System.String MachineName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(50)]
        public virtual System.String IpAddress { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Double? DriveSpaceAvailable { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Double? DriveTotalSpace { get; set; }


        public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }

        #region IEquatable<PerformanceStatisticBase> Members

        public virtual bool Equals(PerformanceStatistic other)
        {
            if (this.PerformanceStatisticId == other.PerformanceStatisticId && this.Cpu == other.Cpu && this.Memory == other.Memory && this.CreationDate == other.CreationDate && this.MachineName == other.MachineName && this.IpAddress == other.IpAddress && this.DriveSpaceAvailable == other.DriveSpaceAvailable && this.DriveTotalSpace == other.DriveTotalSpace)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual void CopyFrom(PerformanceStatistic other)
        {
            if (other != null)
            {
                this.PerformanceStatisticId = other.PerformanceStatisticId;
                this.Cpu = other.Cpu;
                this.Memory = other.Memory;
                this.CreationDate = other.CreationDate;
                this.MachineName = other.MachineName;
                this.IpAddress = other.IpAddress;
                this.DriveSpaceAvailable = other.DriveSpaceAvailable;
                this.DriveTotalSpace = other.DriveTotalSpace;
            }


        }

        #endregion



    }


}
