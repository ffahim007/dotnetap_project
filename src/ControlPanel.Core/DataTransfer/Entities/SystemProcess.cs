﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ControlPanel.Core.Entities
{
    [DataContract]
    [Table("CP_SYSTEMPROCESS")]
    public partial class SystemProcess : IEquatable<SystemProcess>
    {

        [DataMember(EmitDefaultValue = false)]
        [Key]
        public virtual System.Int32 SystemProcessId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(50)]
        [Required]
        public virtual System.String Name { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(100)]
        public virtual System.String Description { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public virtual System.Boolean Enabled { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32 DisplayOrder { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(100)]
        public virtual System.String Ip { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? Port { get; set; }


        [NotMapped]
        public List<SystemProcessThread> SystemProcessThreadList { get; set; }

        public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }

        #region IEquatable<SystemProcessBase> Members

        public virtual bool Equals(SystemProcess other)
        {
            if (this.SystemProcessId == other.SystemProcessId && this.Name == other.Name && this.Description == other.Description && this.Enabled == other.Enabled && this.DisplayOrder == other.DisplayOrder && this.Ip == other.Ip && this.Port == other.Port)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual void CopyFrom(SystemProcess other)
        {
            if (other != null)
            {
                this.SystemProcessId = other.SystemProcessId;
                this.Name = other.Name;
                this.Description = other.Description;
                this.Enabled = other.Enabled;
                this.DisplayOrder = other.DisplayOrder;
                this.Ip = other.Ip;
                this.Port = other.Port;
            }


        }

        #endregion



    }


}
