﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ControlPanel.Core.Entities
{
    [DataContract]
    [Table("CP_SYSTEMEVENTLOG")]
    public partial class SystemEventLog : IEquatable<SystemEventLog>
    {

        [DataMember(EmitDefaultValue = false)]
        [Key]
        public virtual System.Int32 EventLogId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Int32? EventCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(500)]
        public virtual System.String Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.String Description { get; set; }

        [IgnoreDataMember]
        public virtual System.DateTime DateOccured { get; set; }


        [DataMember(EmitDefaultValue = false)]
        [NotMapped]
        public virtual string DateOccuredStr
        {
            get { return DateOccured.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { DateOccured = date.ToUniversalTime(); } }
        }

        [DataMember(EmitDefaultValue = false)]
        public virtual System.Guid? ElmahErrorId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [MaxLength(60)]
        public virtual System.String Source { get; set; }


        public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }

        #region IEquatable<SystemEventLogBase> Members

        public virtual bool Equals(SystemEventLog other)
        {
            if (this.EventLogId == other.EventLogId && this.EventCode == other.EventCode && this.Title == other.Title && this.Description == other.Description && this.DateOccured == other.DateOccured && this.ElmahErrorId == other.ElmahErrorId && this.Source == other.Source)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual void CopyFrom(SystemEventLog other)
        {
            if (other != null)
            {
                this.EventLogId = other.EventLogId;
                this.EventCode = other.EventCode;
                this.Title = other.Title;
                this.Description = other.Description;
                this.DateOccured = other.DateOccured;
                this.ElmahErrorId = other.ElmahErrorId;
                this.Source = other.Source;
            }


        }

        #endregion



    }


}
