﻿using ControlPanel.Core;
using ControlPanel.Core.Argument;
using ControlPanel.Repository;
using System;
using System.Collections.Generic;


namespace MMS.ProcessThreads
{
    public class DeleteDataThread : ISystemProcessThread
    {
        private string _threadName;
        static Queue<double> cpuReading = new Queue<double>();
        DateTime lastAlertSendDateTime = DateTime.UtcNow.AddMinutes(-10);
        public string ThreadName
        {
            get
            {
                return this._threadName;
            }
            set
            {
                this._threadName = value;
            }
        }

        private int? _startRange;
        public int? StartRange
        {
            get
            {
                return this._startRange;
            }
            set
            {
                this._startRange = value;
            }
        }

        private int? _endRange;
        public int? EndRange
        {
            get
            {
                return this._endRange;
            }
            set
            {
                this._endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;
        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return this._lastSuccessfullyExecuted;
            }
            set
            {
                this._lastSuccessfullyExecuted = value;
            }
        }

        private int? _scheduleTime;
        public int? ScheduledTime
        {
            get
            {
                return this._scheduleTime;
            }
            set
            {
                this._scheduleTime = value;
            }
        }

        public string Initialize()
        {

            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            PerformanceStatisticRepository pStatisticRepo = new PerformanceStatisticRepository();
            SystemEventLogRepository sysEventLog = new SystemEventLogRepository();
            FlushData arg = JSONhelper.GetObject<FlushData>(argument);

            try
            {
                int daysAgoPerformance = Convert.ToInt32(arg.PerformanceStatisticDays);
                DateTime dtPerformance = pStatisticRepo.GetMaxCreationDate();
                dtPerformance = dtPerformance.AddDays(-daysAgoPerformance);
                pStatisticRepo.FlushPerformanceStatistic(dtPerformance);

                int daysAgoSystemEvent = Convert.ToInt32(arg.SystemEventLogDays);
                DateTime dtSystemEvt = sysEventLog.GetMaxCreationDate();
                dtSystemEvt = dtSystemEvt.AddDays(-daysAgoSystemEvent);
                sysEventLog.FlushSystemEventLog(dtSystemEvt);

                return "Success";
            }
            catch
            {
                return "Error";
            }
        }
    }
}
