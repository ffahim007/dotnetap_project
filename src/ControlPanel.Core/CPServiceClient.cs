﻿using ControlPanel.Core.DataTransfer;
using ControlPanel.Core.Entities;
using ControlPanel.Service;
using System;
using System.Collections.Generic;

namespace ControlPanel.Core
{
    public class CPServiceClient
    {
        public SystemProcess GetSystemProcessByName(string processName)
        {
            SystemProcessService systemProcessService = new SystemProcessService();
            DataTransfer<ControlPanel.Core.DataTransfer.SystemProcess.GetOutput> dt = systemProcessService.GetSystemProcessByName(processName);
            if (dt != null && dt.IsSuccess)
            {
                SystemProcess process = new SystemProcess();
                process.CopyFrom(dt.Data);
                return process;
            }

            return null;
        }

        public SystemProcessThread UpdateSystemProcessThread(SystemProcessThread processThread)
        {
            SystemProcessThreadService systemProcessThreadService = new SystemProcessThreadService();
            return systemProcessThreadService.UpdateSystemProcessThread(processThread);
        }

        public SystemProcessThread GetSystemProcessThreadByName(string threadName)
        {
            SystemProcessThreadService systemProcessThreadService = new SystemProcessThreadService();
            return systemProcessThreadService.GetSystemProcessThreadByName(threadName);
        }

        public List<SystemProcessThread> GetSystemProcessThreadsByProcessName(string processName)
        {
            SystemProcessService systemProcessService = new SystemProcessService();
            return systemProcessService.GetSystemProcessThreadsByProcessName(processName);
        }

        public List<SystemProcess> GetAllSystemProcess()
        {
            SystemProcessService systemProcessService = new SystemProcessService();
            DataTransfer<List<DataTransfer.SystemProcess.GetOutput>> result = systemProcessService.GetAll();
            List<SystemProcess> processes = new List<SystemProcess>();
            if (result.IsSuccess && result.Data != null && result.Data.Count > 0)
            {
                processes.CopyFrom(result.Data);
            }

            return processes;
        }

        public List<SystemProcessThread> GetSystemProcessThreadsByProcessID(int processID)
        {
            SystemProcessThreadService systemProcessThreadService = new SystemProcessThreadService();
            return systemProcessThreadService.GetSystemProcessThreadByProcessID(processID);
        }

        public bool ToggleSystemProcessThreadContinuous(int systemProcessThreadID)
        {
            SystemProcessThreadService systemProcessThreadService = new SystemProcessThreadService();
            return systemProcessThreadService.ToggleSystemProcessThreadContinuous(systemProcessThreadID);
        }

        public bool ToggleSystemProcessThreadEnabled(int systemProcessThreadID)
        {
            SystemProcessThreadService systemProcessThreadService = new SystemProcessThreadService();
            return systemProcessThreadService.ToggleSystemProcessThreadEnabled(systemProcessThreadID);
        }

        public DataTransfer<List<SystemProcessThread>> GetSystemProcessThreadsByLastUpdateDate(DateTime lastUpdateDate)
        {
            SystemProcessThreadService systemProcessThreadService = new SystemProcessThreadService();
            return systemProcessThreadService.GetSystemProcessThreadsByLastUpdateDate(lastUpdateDate);
        }

        public List<SystemEventLog> GetEventLogByLastUpdateDate(DateTime lastUpdateDate)
        {
            SystemEventLogService systemProcessThreadService = new SystemEventLogService();
            return systemProcessThreadService.GetSystemEventLogsByLastUpdateDate(lastUpdateDate);
        }
    }
}
