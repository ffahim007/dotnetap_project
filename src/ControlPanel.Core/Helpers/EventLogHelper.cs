﻿using ControlPanel.Core.Enums;
using ControlPanel.Service;

namespace ControlPanel.Core.Helpers
{
    public class EventLogHelper
    {

        public static void Log(string str)
        {
            Log(str, string.Empty);
        }


        public static void Log(string str, string description)
        {
            SystemEventLogService logService = new SystemEventLogService();
            logService.InsertSystemEventLog(str, description, EventCodes.Log);
        }

        public static void LogError(string str)
        {
            LogError(str, string.Empty);
        }

        public static void LogError(string str, string description)
        {
            SystemEventLogService logService = new SystemEventLogService();
            logService.InsertSystemEventLog(str, description, EventCodes.Error);
        }
    }
}
