namespace ControlPanel.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ControlPanel.Core.Context.ControlPanelContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ControlPanel.Core.Context.ControlPanelContext context)
        {
            #region System Process
            if (context.SystemProcess.FirstOrDefault(x => x.Name == "FinanceHouseService") == null)
            {
                Entities.SystemProcess entity = new Entities.SystemProcess();

                entity.Name = "FinanceHouseService";
                entity.Description = "FinanceHouseService";
                entity.Enabled = true;
                entity.DisplayOrder = 1;

                context.SystemProcess.Add(entity);
            }
            #endregion

            #region Email Notification Sending Thread
            if (context.SystemProcessThread.FirstOrDefault(x => x.Name == "NotificationThread") == null)
            {
                Entities.SystemProcessThread thread = new Entities.SystemProcessThread();

                thread.Argument = @"4";
                thread.TenantId = null;
                thread.SystemProcessId = 1;
                thread.Name = "NotificationThread";
                thread.SpringEntryName = null;
                thread.Description = null;
                thread.Enabled = true;
                thread.Continuous = true;
                thread.SleepTime = 5;
                thread.AutoStart = true;
                thread.Status = "Sleeping";
                thread.ScheduledTime = null;
                thread.StartRange = null;
                thread.EndRange = null;
                thread.LastSuccessfullyExecuted = new DateTime(2016, 1, 1);
                thread.ContinuousDelay = 5;
                thread.IsDeleted = false;
                thread.DisplayOrder = "1";
                thread.LastUpdateDate = new DateTime(2016, 1, 1);
                thread.ExecutionTime = 184.0105;
                thread.EstimatedExecutionTime = null;
                thread.ShowUpdateInLog = null;
                thread.Class = "NotificationThread";
                thread.Namespace = "FinanceHouse.Service.Thread";
                thread.Assembly = "FinanceHouse.Service";

                context.SystemProcessThread.Add(thread);
            }

            if (context.SystemProcessThread.FirstOrDefault(x => x.Name == "EmailNotificationThread") == null)
            {
                Entities.SystemProcessThread thread = new Entities.SystemProcessThread();

                thread.Argument = @"4";
                thread.TenantId = null;
                thread.SystemProcessId = 1;
                thread.Name = "EmailNotificationThread";
                thread.SpringEntryName = null;
                thread.Description = null;
                thread.Enabled = true;
                thread.Continuous = true;
                thread.SleepTime = 5;
                thread.AutoStart = true;
                thread.Status = "Sleeping";
                thread.ScheduledTime = null;
                thread.StartRange = null;
                thread.EndRange = null;
                thread.LastSuccessfullyExecuted = new DateTime(2016, 1, 1);
                thread.ContinuousDelay = 5;
                thread.IsDeleted = false;
                thread.DisplayOrder = "1";
                thread.LastUpdateDate = new DateTime(2016, 1, 1);
                thread.ExecutionTime = 184.0105;
                thread.EstimatedExecutionTime = null;
                thread.ShowUpdateInLog = null;
                thread.Class = "EmailNotificationThread";
                thread.Namespace = "FinanceHouse.Service.Thread";
                thread.Assembly = "FinanceHouse.Service";

                context.SystemProcessThread.Add(thread);
            }
            #endregion

            #region User Attendance Thread
            if (context.SystemProcessThread.FirstOrDefault(x => x.Name == "UserAttendanceThread") == null)
            {
                Entities.SystemProcessThread thread = new Entities.SystemProcessThread();

                thread.Argument = @"4";
                thread.TenantId = null;
                thread.SystemProcessId = 1;
                thread.Name = "UserAttendanceThread";
                thread.SpringEntryName = null;
                thread.Description = null;
                thread.Enabled = true;
                thread.Continuous = true;
                thread.SleepTime = 120;
                thread.AutoStart = true;
                thread.Status = "Sleeping";
                thread.ScheduledTime = null;
                thread.StartRange = null;
                thread.EndRange = null;
                thread.LastSuccessfullyExecuted = new DateTime(2016, 1, 1);
                thread.ContinuousDelay = 120;
                thread.IsDeleted = false;
                thread.DisplayOrder = "3";
                thread.LastUpdateDate = new DateTime(2016, 1, 1);
                thread.ExecutionTime = 184.0105;
                thread.EstimatedExecutionTime = null;
                thread.ShowUpdateInLog = null;
                thread.Class = "UserAttendanceThread";
                thread.Namespace = "FinanceHouse.Service.Thread";
                thread.Assembly = "FinanceHouse.Service";
                context.SystemProcessThread.Add(thread);
            }
            #endregion
        }
    }
}
