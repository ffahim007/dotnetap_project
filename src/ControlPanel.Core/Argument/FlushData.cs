﻿namespace ControlPanel.Core.Argument
{
    public class FlushData
    {
        public int PerformanceStatisticDays { get; set; }

        public int SystemEventLogDays { get; set; }
    }
}
