﻿using ControlPanel.Core.Context;
using ControlPanel.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ControlPanel.Repository
{

    public partial class PerformanceStatisticRepository
	{
        public PerformanceStatistic InsertPerformanceStatistic(PerformanceStatistic entity)
        {
            using (var context = new ControlPanelContext())
            {
                context.PerformanceStatistic.Add(entity);
                context.SaveChanges();
                return entity;
            }
               
        }

        public List<PerformanceStatistic> GetPerformanceStatisticThreadByLastUpdatedDate(DateTime CreationDate)
        {
            using (var context = new ControlPanelContext())
            {
                return context.PerformanceStatistic.Where(x => x.CreationDate >= CreationDate).ToList();
            }
        }


        public DateTime GetMaxCreationDate()
        {
            using (var context = new ControlPanelContext())
            {
                var date= context.PerformanceStatistic.Max(x=>x.CreationDate);
                if(date.HasValue)
                {
                    return date.Value;
                }
                else
                {
                    return new DateTime();
                }
            }
        }

        public int FlushPerformanceStatistic(DateTime dt)
        {
            using (var context = new ControlPanelContext())
            {
                var list = context.PerformanceStatistic.Where(x => x.CreationDate < dt).ToList();
                if(list!=null)
                {
                    foreach(var item in list)
                    {
                        context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                    }
                    context.SaveChanges();
                    return list.Count;    
                }
                return 0;
            }
           
        }
	}
	
	
}
