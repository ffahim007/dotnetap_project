﻿using ControlPanel.Core.Context;
using ControlPanel.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace ControlPanel.Repository
{

    public partial class SystemEventLogRepository
	{

        public List<SystemEventLog> GetSystemEventLogsByLastUpdateDate(DateTime lastUpdateDate)
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemEventLog.Where(x => x.DateOccured > lastUpdateDate).ToList();
            }
             
        }

        public int FlushSystemEventLog(DateTime dt)
        {
            using (var context = new ControlPanelContext())
            {
                var list = context.SystemEventLog.Where(x => x.DateOccured<dt).ToList();
                if (list != null)
                {
                    foreach (var item in list)
                    {
                        context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                    }
                    context.SaveChanges();
                    return list.Count;
                }
                return 0;
            }

        }

        public List<SystemEventLog> GetAllSystemEventLog()
        {
            using (var context = new ControlPanelContext())
            {
                return  context.SystemEventLog.ToList();
               
            }
        }

        public SystemEventLog InsertSystemEventLog(SystemEventLog entity)
        {
            using (var context = new ControlPanelContext())
            {
                context.SystemEventLog.Add(entity);
                context.SaveChanges();
                return entity;
            }
        }

        public bool DeleteSystemEventLog(int eventLogId)
        {
            using (var context = new ControlPanelContext())
            {
                var item = context.SystemEventLog.Where(x => x.EventLogId == eventLogId).FirstOrDefault();
                if (item != null)
                {
                    context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public SystemEventLog UpdateSystemEventLog(SystemEventLog entity)
        {
            using (var context = new ControlPanelContext())
            {
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
                return entity;
            }
        }

        public SystemEventLog GetSystemEventLog(int eventLogId)
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemEventLog.Where(x => x.EventLogId == eventLogId).FirstOrDefault();
                
            }
        }

        public DateTime GetMaxCreationDate()
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemEventLog.Max(x => x.DateOccured);
            }
        }
    }
	
	
}
