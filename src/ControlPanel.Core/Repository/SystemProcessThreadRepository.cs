﻿using ControlPanel.Core.Context;
using ControlPanel.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ControlPanel.Repository
{

    public partial class SystemProcessThreadRepository
	{

        public SystemProcessThread InsertSystemProcessThread(SystemProcessThread entity)
        {
            using (var context = new ControlPanelContext())
            {
                entity.LastUpdateDate = DateTime.UtcNow;
                context.SystemProcessThread.Add(entity);
                context.SaveChanges();
                return entity;
            }
        }

        public SystemProcessThread UpdateSystemProcessThread(SystemProcessThread entity)
        {
            using (var context = new ControlPanelContext())
            {
                entity.LastUpdateDate = DateTime.UtcNow;
                context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                return entity;
            }
        }

        public SystemProcessThread GetSystemProcessThreadByName(string threadName)
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemProcessThread.Where(x => x.Name == threadName).FirstOrDefault();
            }
            
        }

        public List<SystemProcessThread> GetSystemProcessThreadByProcessID(int processID)
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemProcessThread.Where(x => x.SystemProcessId == processID).ToList();
            }
        }


        public bool ToggleSystemProcessThreadContinuous(int systemProcessThreadID)
        {
            using (var context = new ControlPanelContext())
            {
                var entity = context.SystemProcessThread.Where(x => x.SystemProcessThreadId == systemProcessThreadID).FirstOrDefault();
                if (entity != null)
                {
                    entity.LastUpdateDate = DateTime.UtcNow;
                    entity.Continuous = !entity.Continuous;
                    context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                    return true;
                }
                return false;
            }

          
        }

        public bool ToggleSystemProcessThreadEnabled(int systemProcessThreadID)
        {
            using (var context = new ControlPanelContext())
            {
                var entity = context.SystemProcessThread.Where(x => x.SystemProcessThreadId == systemProcessThreadID).FirstOrDefault();
                if (entity != null)
                {
                    entity.LastUpdateDate = DateTime.UtcNow;
                    entity.Enabled = !entity.Enabled;
                    context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
          
        }

        public List<SystemProcessThread> GetAllSystemProcessThread()
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemProcessThread.ToList();
            }
        }

        public SystemProcessThread GetSystemProcessThreadsByTenantId(int TenantId , string connectionString = null)
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemProcessThread.Where(x => x.TenantId == TenantId).FirstOrDefault();
            }

        }

        public List<SystemProcessThread> GetSystemProcessThreadsByLastUpdateDate(DateTime lastUpdateDate)
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemProcessThread.Where(x => x.LastUpdateDate>lastUpdateDate).ToList();
            }
        }

        public SystemProcessThread GetSystemProcessThreadBySpringName(string springName, string connectionString = null)
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemProcessThread.Where(x => x.SpringEntryName.ToLower()== springName.ToLower()).FirstOrDefault();
            }
        }

       

        public virtual SystemProcessThread UpdateSystemProcessThread(SystemProcessThread entity, string connectionString)
        {
            using (var context = new ControlPanelContext())
            {
                    entity.LastUpdateDate = DateTime.UtcNow;
                    context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                    return entity;
            }
           
        }

        public virtual SystemProcessThread GetSystemProcessThread(System.Int32 SystemProcessThreadId, string connectionString = null)
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemProcessThread.Where(x => x.SystemProcessThreadId == SystemProcessThreadId).FirstOrDefault();
            }

           
        }
        
        public virtual SystemProcessThread InsertSystemProcessThread(SystemProcessThread entity, string connectionString)
        {
            using (var context = new ControlPanelContext())
            {
                entity.LastUpdateDate = DateTime.UtcNow;
                context.SystemProcessThread.Add(entity);
                context.SaveChanges();
                return entity;
            }
        }

        public bool DeleteSystemProcessThread(int v)
        {
            using (var context = new ControlPanelContext())
            {
                var item = context.SystemProcessThread.Where(x => x.SystemProcessThreadId == v).FirstOrDefault();
                if (item != null)
                {
                    context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public List<SystemProcessThread> GetSystemProcessThreadBySystemProcessId(int systemProcessId)
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemProcessThread.Where(x => x.SystemProcessId== systemProcessId).ToList();
            }
        }
    }
	
	
}
