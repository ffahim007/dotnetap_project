﻿using ControlPanel.Core.Context;
using ControlPanel.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ControlPanel.Repository
{

    public partial class DriveStatisticRepository
	{


        public DateTime GetMaxCreationDate()
        {
            using (var context = new ControlPanelContext())
            {
                var date= context.DriveStatistic.Max(x => x.CreationDate);
                if(date.HasValue)
                {
                    return date.Value;
                }    
                else
                {
                    return new DateTime();
                }
            }
         
        }

       

        public List<DriveStatistic> GetPerformanceDrivesStatisticThreadByLastUpdatedDate(DateTime CreationDate)
        {
            using (var context = new ControlPanelContext())
            {
                return context.DriveStatistic.Where(x => x.CreationDate >= CreationDate).ToList();

            }
        }

        public void InsertDriveStatistic(DriveStatistic dS)
        {
            using (var context = new ControlPanelContext())
            {
                context.DriveStatistic.Add(dS);
                context.SaveChanges();

            }
        }
    }
	
	
}
