﻿using ControlPanel.Core.Context;
using ControlPanel.Core.Entities;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace ControlPanel.Repository
{

    public partial class SystemProcessRepository
	{
    
        public SystemProcess GetSystemProcessByName(string name)
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemProcess.Where(x => x.Name.ToLower() == name.ToLower()).FirstOrDefault();
            }
    
        }

       

        public List<SystemProcessThread> GetSystemProcessThreadsByProcessName(string name)
        {
            SystemProcess process = GetSystemProcessByName(name);
            //ISystemProcessThreadRepository systemProcessThreadRepository = IoC.Resolve<ISystemProcessThreadRepository>("SystemProcessThreadRepository");
            SystemProcessThreadRepository systemProcessThreadRepository = new SystemProcessThreadRepository();
            return systemProcessThreadRepository.GetSystemProcessThreadBySystemProcessId(process.SystemProcessId);
        }

        public SystemProcess UpdateSystemProcess(SystemProcess entity)
        {
            using (var context = new ControlPanelContext())
            {
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
                return entity;
            }
        }

        public SystemProcess GetSystemProcess(int systemProcessId)
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemProcess.Where(x=>x.SystemProcessId==systemProcessId).FirstOrDefault();
            }
        }

        public List<SystemProcess> GetAllSystemProcess()
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemProcess.ToList();
            }
        }

        public bool DeleteSystemProcess(int systemProcessId)
        {
            using (var context = new ControlPanelContext())
            {
                var item = context.SystemProcess.Where(x => x.SystemProcessId == systemProcessId).FirstOrDefault();
                if (item != null)
                {
                    context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public SystemProcess InsertSystemProcess(SystemProcess sysprocesentity)
        {
            using (var context = new ControlPanelContext())
            {
                context.SystemProcess.Add(sysprocesentity);
                context.SaveChanges();
                return sysprocesentity;
            }
        }

        public List<SystemProcess> GetSystemProcessByIp(string ip)
        {
            using (var context = new ControlPanelContext())
            {
                return context.SystemProcess.Where(x => x.Ip == ip).ToList();
            }
        }
    }
}
