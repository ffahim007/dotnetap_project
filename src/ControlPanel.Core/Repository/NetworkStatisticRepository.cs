﻿using ControlPanel.Core.Context;
using ControlPanel.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ControlPanel.Repository
{

    public partial class NetworkStatisticRepository
	{

        public NetworkStatistic InsertNetworkStatistic(NetworkStatistic entity)
        {
            using (var context = new ControlPanelContext())
            {
                context.NetworkStatistic.Add(entity);
                context.SaveChanges();
                return entity;

            }
        }


        public List<NetworkStatistic> GetNetworkPerformanceStatisticThreadByLastUpdatedDate(DateTime CreationDate)
        {
            using (var context = new ControlPanelContext())
            {
                return context.NetworkStatistic.Where(x => x.CreationDate >= CreationDate).ToList();

            }

        }


        public DateTime GetMaxCreationDate()
        {
            using (var context = new ControlPanelContext())
            {
                var date= context.NetworkStatistic.Max(x=>x.CreationDate);
                return date;

            }
        }


        public List<NetworkStatistic> GetAllSystemNetworkPerformanceByCreationDate(DateTime CreationDate)
        {
            using (var context = new ControlPanelContext())
            {
                return context.NetworkStatistic.Where(x=>x.CreationDate>=CreationDate).ToList();

            }
        }

        public List<NetworkStatistic> GetAllSystemNetworkPerformanceByAllDate()
        {
            using (var context = new ControlPanelContext())
            {
                return context.NetworkStatistic.ToList();

            }
        }
	}
	
	
}
