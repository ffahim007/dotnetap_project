﻿using Core.Entity;
using Core.Infrastructure;
using Core.IRepository;
using Recipe.Core.Base.Generic;

namespace Repository
{
    public class PermissionRepository : AuditableRepository<Permission, int>, IPermissionRepository
    {
        private ISystemRequestInfo requestInfo;

        public PermissionRepository(ISystemRequestInfo requestInfo)
            : base(requestInfo)
        {
            this.requestInfo = requestInfo;
        }
    }
}
