﻿using System.Data.Entity;
using System.Linq;
using Core.Entity;
using Core.Infrastructure;
using Core.IRepository;
using Recipe.Core.Base.Generic;

namespace Repository
{
    public class RoleRepository : AuditableRepository<ApplicationRole, string>, IRoleRepository
    {
        private ApplicationRoleManager roleManager;
        private ISystemRequestInfo requestInfo;

        public RoleRepository(ISystemRequestInfo requestInfo, ApplicationRoleManager roleManager)
            : base(requestInfo)
        {
            this.requestInfo = requestInfo;
            this.roleManager = new ApplicationRoleManager(new ApplicationRoleStore(requestInfo));
        }

        protected override IQueryable<ApplicationRole> DefaultListQuery
        {
            get
            {
                return base.DefaultListQuery
                           .Include(x => x.UserRolePermission)
                           .Include(x => x.UserRolePermission.Select(y => y.Permission)).OrderByDescending(x => x.Id);
            }
        }

        protected override IQueryable<ApplicationRole> DefaultSingleQuery
        {
            get
            {
                return base.DefaultSingleQuery
                           .Include(x => x.UserRolePermission)
                           .Include(x => x.UserRolePermission.Select(y => y.Permission)).OrderByDescending(x => x.Id);
            }
        }
    }
}
