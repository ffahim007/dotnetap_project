﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.Entity;
using Core.Infrastructure;
using Core.IRepository;
using Recipe.Core.Base.Generic;

namespace Repository
{
    public class UserRolePermissionRepository : Repository<UserRolePermission, int>, IUserRolePermissionRepository
    {
        public UserRolePermissionRepository(ISystemRequestInfo requestInfo)
            : base(requestInfo)
        {
        }

        protected override IQueryable<UserRolePermission> DefaultSingleQuery
        {
            get
            {
                return base.DefaultSingleQuery.Include(x => x.Permission).Include(x => x.Role).OrderByDescending(x => x.Id);
            }
        }

        protected override IQueryable<UserRolePermission> DefaultListQuery
        {
            get
            {
                return base.DefaultListQuery.Include(x => x.Permission).Include(x => x.Role).OrderByDescending(x => x.Id);
            }
        }

        public async Task<List<string>> GetUserPermissions(string roleId)
        {
            return await this.DefaultListQuery.Where(x => x.RoleID == roleId).Select(x => x.Permission.Name).ToListAsync();
        }

        public bool IsUserPermitted(string roleId, List<int> permissionId)
        {
            return this.DefaultListQuery.Any(x => x.RoleID == roleId && permissionId.Contains(x.PermissionID));
        }
    }
}
