using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using Core.Infrastructure;
using Core.IRepository;
using Recipe.Core.Base.Interface;

namespace Repository
{
    public class UnitOfWork : ISystemUnitOfWork
    {
        private readonly ISystemRequestInfo systemRequestInfo;
        private readonly IUserRepository userRepository;
        private readonly IExceptionHelper exceptionHelper;
        private readonly IRoleRepository roleRepository;
        private readonly IRequestInfo requestInfo;
        private readonly IUserRolePermissionRepository userRolePermissionRepository;
        private readonly IPermissionRepository permissionRepository;
        private readonly IUserSessionRepository userSessionRepository;
        //// UOW1

        public UnitOfWork(
            ISystemRequestInfo systemRequestInfo,
            IUserRepository userRepository,
            IExceptionHelper exceptionHelper,
            ISystemRequestInfo requestInfo,
            IUserRolePermissionRepository userRolePermissionRepository,
            IRoleRepository roleRepository,
            IPermissionRepository permissionRepository, //// UOW2
            IUserSessionRepository userSessionRepository)
        {
            this.systemRequestInfo = systemRequestInfo;
            this.userRepository = userRepository;
            this.exceptionHelper = exceptionHelper;
            this.exceptionHelper = exceptionHelper;
            this.roleRepository = roleRepository;
            this.requestInfo = requestInfo;
            this.userRolePermissionRepository = userRolePermissionRepository;
            this.permissionRepository = permissionRepository;
            this.userSessionRepository = userSessionRepository;
            //// UOW3
        }

        public DbContext DBContext
        {
            get
            {
                return this.systemRequestInfo.Context;
            }
        }

        public ISystemRequestInfo SystemRequestInfo
        {
            get
            {
                return this.systemRequestInfo;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                return this.userRepository;
            }
        }

        public IUserSessionRepository UserSessionRepository
        {
            get
            {
                return this.userSessionRepository;
            }
        }

        public IUserRolePermissionRepository UserRolePermissionRepository
        {
            get
            {
                return this.userRolePermissionRepository;
            }
        }

        public IExceptionHelper ExceptionHelper
        {
            get
            {
                return this.exceptionHelper;
            }
        }

        public IRoleRepository RoleRepository
        {
            get
            {
                return this.roleRepository;
            }
        }

        public IPermissionRepository PermissionRepository
        {
            get
            {
                return this.permissionRepository;
            }
        }

        public IRequestInfo RequestInfo
        {
            get
            {
                return this.requestInfo;
            }
        }

        //// UOW4

        public async Task<int> SaveAsync()
        {
            try
            {
                return await this.DBContext.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                this.exceptionHelper.ThrowAPIException(e.EntityValidationErrors.First().ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return 0;
        }

        public int Save()
        {
            try
            {
                return this.DBContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public System.Data.Entity.DbContextTransaction BeginTransaction()
        {
            return this.DBContext.Database.BeginTransaction();
        }
    }
}
