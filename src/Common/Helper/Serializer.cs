﻿using System;
using System.Net;

namespace Common.Helper
{
    public class Serializer
    {
        public static dynamic CreateObject(HttpStatusCode code, string message, object obj)
        {
            var successFlag = false;

            if (code.ToString() == "OK")
            {
                successFlag = true;
            }

            dynamic response = new { success = successFlag, meta = new { code = code, message = message }, data = obj };
            return response;
        }

        public static dynamic CreateObject(HttpStatusCode code, string message, object obj, string dateTime)
        {
            var successFlag = false;

            if (code.ToString() == "OK")
            {
                successFlag = true;
            }

            dynamic response = new { meta = new { code = code, message = message, date = dateTime }, data = obj };
            return response;
        }
    }
}
