﻿using Core.Constant;
using Core.DTO;
using Core.Enum;
using Core.IService;
using System;

namespace Test.Mock
{
    public static class UserMock
    {
        public static UserDTO GetUser()
        {
            var user = new UserDTO
            {
                Fullname = "Unit Test",
                Password = "123456",
                UserName = "unit1.test1@test.com",
                Email = "unit1.test1@test.com",
                RoleId = Roles.GetRoleId(UserRoles.Admin),
                AccountRole = "Account Role Test",
                Address = "Address Test",
                CellNumber = "03219876543",
                LandLineNumber = "02139876543"
            };
            return user;
        }

        public static UserDTO CreateUser(IUserService userService)
        {
            var user = GetUser();
            var dbUser = userService.CreateAsync(user).Result;
            return dbUser;
        }

        public static string GetToken()
        {
            string token = "f9c25cbd-e134-4711-b871-e09067aa5808";
            return token;
        }
    }
}
