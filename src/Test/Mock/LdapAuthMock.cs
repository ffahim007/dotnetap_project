﻿using Core.DTO;
using Core.Enum;

namespace Test.Mock
{
    public class LdapAuthMock
    {

        public static AuthDTO LdapAuthDTO()
        {
            var authdto = new AuthDTO()
            {
                EmailId = "fh_saifullah.iqbal",
                Password = "10pearls"
            };

            return authdto;
        }
    }
}
