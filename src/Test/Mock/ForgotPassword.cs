﻿using Core.DTO;

namespace Test.Mock
{
    public static class ForgotPassword
    {
        public static string GetResetPasswordToken()
        {
            var resetPasswordToken = new ResetPasswordLinkDTO
            {
                UserName = "admin_10Pearls"
            };
            return resetPasswordToken.UserName;
        }

        public static ForgotPasswordDTO ResetPassword(out string passwordToken)
        {
            var forgotPassword = new ForgotPasswordDTO
            {
                NewPassword = "10Pearls"
            };
            passwordToken = "f9c25cbd-e134-4711-b871-e09067aa5808";
            return forgotPassword;
        }
    }
}
