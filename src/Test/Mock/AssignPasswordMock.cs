﻿using Core.DTO;

namespace Test.Mock
{
    public static class AssignPasswordMock
    {
        public static AssignPasswordDTO AssignPassword()
        {
            var assignPassword = new AssignPasswordDTO()
            {
                EmailId = "admin",
                Password = "10pearls"
            };

            return assignPassword;
        }
    }
}
