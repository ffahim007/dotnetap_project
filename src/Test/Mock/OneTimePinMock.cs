﻿using Core.DTO;

namespace Test.Mock
{
    public static class OneTimePinMock
    {
        public static OneTimePinDTO GetOneTimePin()
        {
            var otp = new OneTimePinDTO
            {
                // OneTimePin = "1234",
                // UserName = "admin_10Pearls"

                OneTimePin = "9568",
                EmailId = "admin"
            };
            return otp;
        }
    }
}
