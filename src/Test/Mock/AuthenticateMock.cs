﻿using Core.DTO;

namespace Test.Mock
{
    public static class AuthenticateMock
    {
        public static AuthDTO Authenticate()
        {
            var authenticate = new AuthDTO()
            {
                EmailId = "admin",
                Password = "10pearls",
                PhoneNumber = "353285062132261"
                // AuthType = AuthType.Sql
            };

            return authenticate;
        }
    }
}
