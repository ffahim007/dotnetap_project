﻿using System.Collections.Generic;

namespace Core.DTO
{
    public class ListDTO
    {
        public List<int> Ids { get; set; }
    }
}
