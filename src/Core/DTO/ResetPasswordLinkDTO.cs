﻿using System.ComponentModel.DataAnnotations;
using Core.Entity;

namespace Core.DTO
{
    public class ResetPasswordLinkDTO
    {
        [Required]
        [RegularExpression(Constant.Validations.EmailAddress, ErrorMessage = "")]
        public string UserName { get; set; }
    }
}
