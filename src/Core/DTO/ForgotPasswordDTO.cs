﻿using System.ComponentModel.DataAnnotations;
using Core.Entity;

namespace Core.DTO
{
    public class ForgotPasswordDTO
    {
        [Required]
        [MinLength(5)]
        [MaxLength(20)]
        public string NewPassword { get; set; }
    }
}
