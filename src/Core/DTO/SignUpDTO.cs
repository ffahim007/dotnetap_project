﻿using Newtonsoft.Json.Linq;

namespace Core.DTO
{
    public class SignUpDTO
    {
        public JObject Auth { get; set; }
    }
}
