﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Core.Constant;
using Core.Entity;
using Core.Enum;
using Recipe.Core.Base.Abstract;

namespace Core.DTO
{
    public class UserDTO : DTO<ApplicationUser, string>
    {
        public UserDTO()
        {
        }

        public UserDTO(ApplicationUser entity) : base(entity)
        {
        }

        public string Fullname { get; set; }

        public string UserName { get; set; }

        public string ClientId { get; set; }

        public string Email { get; set; }

        public string SecondaryEmail { get; set; }

        public string Password { get; set; }

        public bool IsLocked { get; set; }

        public string Role { get; set; }

        [Required]
        public string RoleId { get; set; }

        public string AccountRole { get; set; }

        public string CellNumber { get; set; }

        public string LandLineNumber { get; set; }

        public string Bio { get; set; }

        public string Address { get; set; }

        public string CreatedOn { get; set; }

        public string LastModifiedOn { get; set; }

        public string LockoutEndDateUtc { get; set; }

        public string Image { get; set; }

        public string Gender { get; set; }

        public UserStatus Status { get; set; }

        public string DeviceToken { get; set; }

        public string DeviceType { get; set; }

        public List<int> UserSpecialities { get; set; }

        public override ApplicationUser ConvertToEntity(ApplicationUser entity)
        {
            entity = base.ConvertToEntity(entity);

            entity.Fullname = this.Fullname;

            if (!string.IsNullOrEmpty(entity.Id) && !string.IsNullOrEmpty(Roles.GetRole(this.RoleId)))
            {
                entity.Roles.Add(new ApplicationUserRole { RoleId = this.RoleId, UserId = entity.Id });
            }

            entity.Email = this.Email;
            entity.UserName = this.Email;
            entity.EmailConfirmed = true;
            entity.IsBlocked = this.IsLocked;
            entity.CellNumber = this.CellNumber;
            entity.AccountRole = this.AccountRole;
            entity.CreatedOn = DateTime.UtcNow;
            entity.Status = (int)this.Status;
            entity.Bio = this.Bio;
            entity.Gender = this.Gender;
            entity.SecondaryEmail = this.SecondaryEmail;
            entity.Address = this.Address;
            entity.Image = this.Image;
            entity.ClientId = this.ClientId;
            return entity;
        }

        public override void ConvertFromEntity(ApplicationUser entity)
        {
            base.ConvertFromEntity(entity);
            this.Fullname = entity.Fullname;
            this.UserName = entity.UserName;
            this.Email = entity.Email;
            this.IsLocked = entity.IsBlocked;

            if (entity.Roles != null && entity.Roles.Count > 0)
            {
                this.Role = Roles.GetRole(this.RoleId);
                if (entity.Roles.Count > 0)
                {
                    this.RoleId = entity.Roles.FirstOrDefault().RoleId;
                }
            }

            if (string.IsNullOrEmpty(entity.Image))
            {
                this.Image = Path.Combine(Constant.Constant.ImagePath, Constant.Constant.ImagePlaceholder);
            }
            else
            {
                this.Image = entity.Image;
            }

            this.CellNumber = entity.CellNumber;
            this.LandLineNumber = entity.PhoneNumber;
            this.AccountRole = entity.AccountRole;
            this.CreatedOn = entity.CreatedOn.ToString(Validations.DateFormat);
            this.LastModifiedOn = (entity.LastModifiedOn != null) ? entity.LastModifiedOn.ToString(Validations.DateFormat) : string.Empty;
            this.Password = entity.PasswordHash;
            this.Status = (UserStatus)entity.Status;
            this.Gender = entity.Gender;
            this.Bio = entity.Bio;
            this.Address = entity.Address;
            this.ClientId = entity.ClientId;
            this.SecondaryEmail = entity.SecondaryEmail;
            this.LockoutEndDateUtc = entity.LockoutEndDateUtc.HasValue ? entity.LockoutEndDateUtc.Value.ToString(Validations.DateFormat) : null;
        }

        public void ConvertFromEntityForChangeRequest(ApplicationUser entity)
        {
            this.Id = entity.Id;
            this.Fullname = entity.Fullname;
            this.UserName = entity.UserName;
            this.Email = entity.Email;
            this.IsLocked = entity.IsBlocked;
            this.Status = (UserStatus)entity.Status;
            this.Gender = entity.Gender;
            this.Bio = entity.Bio;
            this.Address = entity.Address;
            this.SecondaryEmail = entity.SecondaryEmail;
            this.ClientId = entity.ClientId;

            if (entity.Roles != null && entity.Roles.Count > 0)
            {
                if (entity.Roles.Count > 0)
                {
                    this.RoleId = entity.Roles.FirstOrDefault().RoleId;
                }
            }

            this.CellNumber = entity.CellNumber;
            this.LandLineNumber = entity.PhoneNumber;
        }

        private string SetStatus(int status)
        {
            Enum.UserStatus statusText = (Enum.UserStatus)status;
            return statusText.ToString();
        }
    }
}