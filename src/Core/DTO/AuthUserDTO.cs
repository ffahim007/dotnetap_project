﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Core.Constant;
using Core.Entity;
using Core.Enum;
using Recipe.Core.Base.Abstract;

namespace Core.DTO
{
    public class AuthUserDTO
    {
        public UserDTO User { get; set; }

        public AccessTokenDTO Auth { get; set; }
    }
}