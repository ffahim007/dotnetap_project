﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO
{
    public class CommonDTO
    {
        public string EmailId { get; set; }

        public string Password { get; set; }

        public string DeviceToken { get; set; }
    }
}
