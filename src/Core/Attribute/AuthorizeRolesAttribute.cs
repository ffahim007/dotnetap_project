﻿using System.Web.Http;
using Core.Enum;

namespace Core.Attribute
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params UserRoles[] roles)
            : base()
        {
            this.Roles = string.Join(",", roles);
        }
    }
}
