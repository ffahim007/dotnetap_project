﻿namespace Core.Enum
{
    public enum JobStart
    {
        CannotStart,
        CanStart,
        Exception
    }
}
