﻿namespace Core.Enum
{
    public enum UserRoles
    {
        None = 1,
        Admin = 2,
        User = 3,
        Agency = 4
    }

    public enum UserStatus
    {
        Active = 1,
        Inactive = 0,
        Freeze = 2,
        Blocked = 3
    }

    public enum Gender
    {
        Male = 0,
        Female = 1
    }
}
