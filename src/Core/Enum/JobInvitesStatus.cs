﻿namespace Core.Enum
{
    public enum JobInvitesStatus
    {
        Pending,
        Accepted,
        Rejected,
        Cancelled,
        Expired
    }
}
