﻿namespace Core.Enum
{
    public enum DeviceAssignment
    {
        None,
        Unassigned,
        Assigned
    }
}
