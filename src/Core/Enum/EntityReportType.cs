﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enum
{
    public enum EntityReportType
    {
        CSV = 1,
        PDF = 2
    }
}
