﻿namespace Core.Enum
{
    public enum DocumentStatus
    {
        Expired,
        Valid,
        Warning,
        Pending
    }
}
