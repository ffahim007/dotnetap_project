﻿namespace Core.Enum
{
    public enum UserPermissions
    {
        None,
        SystemUsers,
        ExternalUsers
    }
}
