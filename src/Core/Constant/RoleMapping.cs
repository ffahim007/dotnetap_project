﻿using System.Collections.Generic;
using System.Linq;
using Core.Entity;
using Core.Enum;

namespace Core.Constant
{
    public class RoleMapping
    {
        public ApplicationRole Role { get; set; }

        public List<ApplicationRole> Roles { get; set; }

        private static List<RoleMapping> All
        {
            get
            {
                var roleMapping = new List<RoleMapping>
                {
                    new RoleMapping
                    {
                        Role = new ApplicationRole { Id = Core.Constant.Roles.GetRoleId(UserRoles.Admin), Name = UserRoles.Admin.ToString() },
                        Roles = new List<ApplicationRole>
                        {
                            new ApplicationRole { Id = Core.Constant.Roles.GetRoleId(UserRoles.User), Name = UserRoles.User.ToString() },
                            new ApplicationRole { Id = Core.Constant.Roles.GetRoleId(UserRoles.Agency), Name = UserRoles.Agency.ToString() }
                        }
                    }
                };

                return roleMapping;
            }
        }

        public static RoleMapping GetMapping(UserRoles role)
        {
            var roleMapping = All.FirstOrDefault(x => x.Role.Name == role.ToString());
            return roleMapping;
        }
    }
}
