﻿namespace Core.Constant
{
    public static class Constant
    {
        public const string TypePost = "Post";
        public const string TypeUser = "User";
        public const string TypeJob = "Job";
        public const string TypeNews = "News";
        public const string TypeDailyDevotion = "DailyDevotion";

        public const string Male = "Male";
        public const string Female = "Female";

        public const string Applied = "Apply";
        public const string Accepted = "Accept";
        public const string Saved = "Save";

        public const string Leave = "Leave";
        public const string Join = "Join";

        public const string ImagePlaceholder = "placeholder.png";
        //public const string ImagePath = "http://192.168.29.124:90/Images/";
        public const string ImagePath = "/Images/";
    }
}
