﻿using Core.DBContext;
using Core.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Core.Infrastructure
{
    public class ApplicationRoleStore : RoleStore<ApplicationRole, string, ApplicationUserRole>
    {
        public ApplicationRoleStore(SystemContext context) : base(context)
        {
        }

        public ApplicationRoleStore(ISystemRequestInfo requestInfo) : base(requestInfo.Context)
        {
        }
    }
}
