﻿using Core.DBContext;
using Core.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Core.Infrastructure
{
    public class ApplicationUserStore : UserStore<ApplicationUser, ApplicationRole, string, IdentityUserLogin, ApplicationUserRole, IdentityUserClaim>
    {
        public ApplicationUserStore(ISystemRequestInfo requestInfo) : base(requestInfo.Context)
        {
        }

        public ApplicationUserStore(SystemContext context) : base(context)
        {
        }
    }
}