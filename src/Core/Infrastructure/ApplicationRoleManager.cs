﻿using Core.Entity;
using Microsoft.AspNet.Identity;

namespace Core.Infrastructure
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        public ApplicationRoleManager(ApplicationRoleStore roleStore) : base(roleStore)
        {
        }
    }
}
