﻿using Core.Enum;
using Recipe.Core.Base.Interface;

namespace Core.Infrastructure
{
    public interface ISystemRequestInfo : IRequestInfo
    {
        UserRoles UserRole { get; }
    }
}
