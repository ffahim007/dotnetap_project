﻿using Core.Entity;
using Recipe.Core.Base.Interface;

namespace Core.IRepository
{
    public interface IPermissionRepository : IRepository<Permission, int>
    {
    }
}
