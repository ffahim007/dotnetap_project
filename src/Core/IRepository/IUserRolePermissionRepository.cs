﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entity;
using Recipe.Core.Base.Interface;

namespace Core.IRepository
{
    public interface IUserRolePermissionRepository : IRepository<UserRolePermission, int>
    {
        Task<List<string>> GetUserPermissions(string roleId);

        bool IsUserPermitted(string roleId, List<int> permissionId);
    }
}
