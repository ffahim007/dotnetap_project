using System.Data.Entity;
using System.Threading.Tasks;
using Core.Infrastructure;
using Recipe.Core.Base.Interface;

namespace Core.IRepository
{
    public interface ISystemUnitOfWork : IUnitOfWork
    {
        IRequestInfo RequestInfo { get; }

        IUserRepository UserRepository { get; }

        IExceptionHelper ExceptionHelper { get; }

        IUserRolePermissionRepository UserRolePermissionRepository { get; }

        IRoleRepository RoleRepository { get; }

        IPermissionRepository PermissionRepository { get; }

        IUserSessionRepository UserSessionRepository { get; }

        //// IUOW

        Task<int> SaveAsync();

        int Save();

        DbContextTransaction BeginTransaction();
    }
}