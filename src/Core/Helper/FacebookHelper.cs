﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Core.Helper
{
    public class FacebookHelper
    {
        public readonly HttpClient HttpClient;

        public FacebookHelper()
        {
            this.HttpClient = new HttpClient
            {
                BaseAddress = new Uri("https://graph.facebook.com/v4.0/")
            };

            this.HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<T> GetAsync<T>(string accessToken, string endPoint, string args = null)
        {
            var response = await this.HttpClient.GetAsync(string.Format("{0}assecc_token={1}&{2}", endPoint, accessToken, args));
            if (!response.IsSuccessStatusCode)
            {
                return default(T);
            }

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(result);
        }
    }
}
