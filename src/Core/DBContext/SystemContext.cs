﻿using System.Data.Entity;
using Core.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Core.DBContext
{
    public class SystemContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserLogin, ApplicationUserRole, IdentityUserClaim>
    {
        public SystemContext() : base("DefaultConnectionString")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Permission> Permission { get; set; }

        public DbSet<UserRolePermission> UserRolePermission { get; set; }

        public DbSet<UserSession> UserSession { get; set; }

        ////DBSET
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
