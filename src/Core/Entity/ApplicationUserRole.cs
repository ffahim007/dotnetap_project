﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Core.Entity
{
    public class ApplicationUserRole : IdentityUserRole<string>
    {
        public ApplicationUserRole()
        {
        }

        [Key]
        public int Id { get; set; }
    }
}
