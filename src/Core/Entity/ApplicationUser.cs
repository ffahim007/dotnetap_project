﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;
using Recipe.Core.Base.Interface;

namespace Core.Entity
{
    public class ApplicationUser : IdentityUser<string, IdentityUserLogin, ApplicationUserRole, IdentityUserClaim>, IAuditModel<string>
    {
        public ApplicationUser()
        {
            this.Id = Guid.NewGuid().ToString();
        }

        public string ClientId { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsDeleted { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime LastModifiedOn { get; set; }

        public string Fullname { get; set; }

        public string ResetCode { get; set; }

        public bool IsBlocked { get; set; }

        public string AccountRole { get; set; }

        public string CellNumber { get; set; }

        public int Status { get; set; }

        public string Address { get; set; }

        public string Image { get; set; }

        public string Gender { get; set; }

        public string SecondaryEmail { get; set; }

        public string Bio { get; set; }

        public virtual IList<UserSession> UserSession { get; set; }
    }
}
