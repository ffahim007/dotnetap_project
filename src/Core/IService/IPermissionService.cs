﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTO;
using Core.Entity;
using Core.Enum;
using Core.IRepository;
using Recipe.Core.Attribute;
using Recipe.Core.Base.Interface;
using Recipe.Core.Enum;

namespace Core.IService
{
    public interface IPermissionService : IService<IPermissionRepository, Permission, PermissionDTO, int>
    {
        [AuditOperation(OperationType.Read)]
        Task<PermissionDTO> Get(string name);

        [AuditOperation(OperationType.Read)]
        Task<List<PermissionDTO>> Get(PermissionGroup group);

        [AuditOperation(OperationType.Read)]
        Task<List<PermissionDTO>> GetReportPermissions(string reportName);
    }
}