﻿using System.Collections.Generic;
using Core.DTO;
using Core.Entity;
using Core.IRepository;
using Recipe.Core.Attribute;
using Recipe.Core.Base.Interface;
using Recipe.Core.Enum;

namespace Core.IService
{
    public interface IRoleService : IService<IRoleRepository, ApplicationRole, RoleDTO, string>
    {
        [AuditOperation(OperationType.Read)]
        List<RoleDTO> GetCurrentUserRoleMapping();
    }
}
