﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Core.DTO;
using Core.Entity;
using Core.Enum;
using Core.IRepository;
using Microsoft.AspNet.Identity;
using Recipe.Common.Helper;
using Recipe.Core.Attribute;
using Recipe.Core.Base.Interface;
using Recipe.Core.Enum;

namespace Core.IService
{
    public interface IUserService : IService<IUserRepository, ApplicationUser, UserDTO, string>
    {
        Task<TotalResultDTO<UserDTO>> GetAllUserUpdateAsync(JsonApiRequest request, string search);

        Task<UserDTO> ValidateUserAsync(string userName, string password);

        Task<UserDTO> FindByUserNameAsync(string userName);

        Task<ApplicationUser> FindByClientId(string clientId);

        Task<UserDTO> GetInitialUserDataAsync();

        Task<AuthUserDTO> GetUserData(string userId);

        Task<string> ForgotPassword(string userName);

        Task<string> ForgotPassword(string token, ForgotPasswordDTO dtoObject);

        Task<bool> ValidateTokenAsync(string token);

        Task<string> GetUserRole(string userId);

        Task<ChangePasswordDTO> ChangePasswordAsync(ChangePasswordDTO dtoObject);

        [AuditOperation(OperationType.Create)]
        Task<AssignPasswordDTO> AssignPasswordAsync(AssignPasswordDTO dtoObject);

        Task<ChangePasswordDTO> ChangeUserPasswordAsync(ChangePasswordDTO dtoObject);

        [AuditOperation(OperationType.Update)]
        Task UpdateUserRole(string userId, string roleId);

        [AuditOperation(OperationType.Create)]
        Task<bool> VerifyOneTimePinAsync(OneTimePinDTO dtoObject);

        Task<bool> IsLockedOut(UserDTO dtoObject);

        Task<bool> GetLockoutEnabled(UserDTO dtoObject);

        Task<IdentityResult> AccessFailed(UserDTO dtoObject);

        Task<int> GetAccessFailedCount(UserDTO dtoObject);

        //[AuditOperation(OperationType.Delete)]
        Task DeleteMultipleUsersAsync(List<UserIdDTO> dtoList);

        [AuditOperation(OperationType.Update)]
        Task UpdateUserStatusAsync(List<ChangeUserStatusDTO> dtoList);

        Task<bool> VerifyUserNameWithIMEIForLogin(UserNameDTO dtoObject);

        UserDTO GetUser(string id);

        bool FindByEmail(string email, string userId = null);

        bool IsBlocked();

        Task<LoginDTO> FindByEmailOrMobileAsync(AuthDTO authDto);

        [AuditOperation(OperationType.Create)]
        Task<bool> UnlockUserAsync(string userId);

        Task<bool> UserNameExists(string userName);

        Task<bool> EmailExists(string emailId);

        Task<bool> IdentificationExists(string identificationNumber, int countryId);

        Task<bool> EmailOrMobileExists(string email, string mobileNumber, UserRoles userRole, bool fromWeb);

        Task<List<KeyValuePair<string, object>>> GetUserByRole();

        Task<int> ChangeOnline(bool isOnline, string userId = null);

        Task<UserDTO> GetFacebookAccountAsync(string accessToken);

        Task<List<string>> MultiImageUploadAsync(MultipartMemoryStreamProvider multipart);
    }
}
