﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTO;
using Core.Entity;
using Core.IRepository;
using Recipe.Core.Attribute;
using Recipe.Core.Base.Interface;
using Recipe.Core.Enum;

namespace Core.IService
{
    public interface IUserRolePermissionService : IService<IUserRolePermissionRepository, UserRolePermission, UserRolePermissionDTO, int>
    {
        [AuditOperation(OperationType.Read)]
        bool GetUserPermissionsByRole(string roleId, List<int> permissionId);

        Task<string> GetUserRolePermission(string roleID);
    }
}
