﻿using System.Text.RegularExpressions;

namespace Core.Extensions
{
    public static class StringExtension
    {
        public static string RemoveSpaces(this string reportName)
        {
            return Regex.Replace(reportName, @"\s+", string.Empty);
        }
    }
}
