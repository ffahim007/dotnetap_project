﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Constant;
using Core.DTO;
using Core.Entity;
using Core.Infrastructure;
using Core.IRepository;
using Core.IService;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;

namespace API.Provider
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private IUserService userService;
        private IConfigurationService configurationService;
        private IUserRolePermissionService userRolePermissionService;
        private ApplicationUserManager userManager;
        private ISystemRequestInfo requestInfo;

        public AuthorizationServerProvider(IUserService userService, IConfigurationService configurationService, IUserRolePermissionService userRolePermissionService, ApplicationUserManager userManager, ISystemRequestInfo requestInfo)
        {
            this.userService = userService;
            this.configurationService = configurationService;
            this.userRolePermissionService = userRolePermissionService;
            this.requestInfo = requestInfo;
            this.userManager = new ApplicationUserManager(new ApplicationUserStore(this.requestInfo));
            this.userManager.UserValidator = new UserValidator<ApplicationUser>(this.userManager) { AllowOnlyAlphanumericUserNames = false };
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            UserDTO userDTO = await this.userService.FindByUserNameAsync(context.UserName);
            UserDTO validCredentials = await this.userService.ValidateUserAsync(context.UserName, context.Password);

            if (!context.HasError)
            {
                context.Options.AccessTokenExpireTimeSpan = new TimeSpan(24, 0, 0);
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                var properties = new AuthenticationProperties();
                identity.AddClaim(new Claim(General.ClaimsUserId, validCredentials.Id));
                identity.AddClaim(new Claim(General.ClaimsUserName, validCredentials.UserName));
                identity.AddClaim(new Claim(General.ClaimsFullName, validCredentials.Fullname));
                identity.AddClaim(new Claim(ClaimTypes.Name, validCredentials.UserName));
                identity.AddClaim(new Claim(General.ClaimsEmailId, validCredentials.Email));
                var role = await this.userService.GetUserRole(validCredentials.Id);
                identity.AddClaim(new Claim(ClaimTypes.Role, role));
                var permissions = await this.userRolePermissionService.GetUserRolePermission(userDTO.RoleId);
                identity.AddClaim(new Claim(General.ClaimsPermissions, permissions));
                var ticket = new AuthenticationTicket(identity, properties);
                context.Validated(ticket);
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            return base.TokenEndpoint(context);
        }

        private async Task CheckAccountStatus(UserDTO validCredentials, UserDTO userDTO, OAuthGrantResourceOwnerCredentialsContext context)
        {
            if (userDTO != null && validCredentials == null)
            {
                var isLockout = await this.userService.IsLockedOut(userDTO);
                if (isLockout)
                {
                    context.SetError(Message.UserIvalidGrant, Message.UserAccountLocked);
                    return;
                }
                else if (await this.userService.GetLockoutEnabled(userDTO) && validCredentials == null)
                {
                    await this.userService.AccessFailed(userDTO);

                    string message;

                    if (await this.userService.IsLockedOut(userDTO))
                    {
                        context.SetError(Message.UserIvalidGrant, Message.UserUnusualAttempts);
                        return;
                    }
                    else
                    {
                        int accessFailedCount = await this.userService.GetAccessFailedCount(userDTO);

                        int attemptsLeft = Convert.ToInt32(this.configurationService.MaxPasswordRetryCount) - accessFailedCount;

                        message = string.Format(Message.UserSignInAttemptsLeft, attemptsLeft);
                        if (accessFailedCount >= 3)
                        {
                            await this.userManager.AccessFailedAsync(userDTO.Id);
                        }

                        context.SetError(Message.UserIvalidGrant, message);
                        return;
                    }
                }
                else
                {
                    context.SetError(Message.UserIvalidGrant, Message.UserInvalidUserNameOrPassword);
                    return;
                }
            }
            else if (userDTO == null && validCredentials == null)
            {
                context.SetError(Message.UserIvalidGrant, Message.UserInvalidUserNameOrPassword);
                return;
            }
            else if (await this.userService.IsLockedOut(userDTO))
            {
                context.SetError(Message.UserIvalidGrant, Message.UserAccountLocked);
                return;
            }
            else
            {
                if (validCredentials == null)
                {
                    context.SetError(Message.UserIvalidGrant, Message.UserInvalidUserNameOrPassword);
                    return;
                }

                //this.userManager = new ApplicationUserManager(new ApplicationUserStore(this.requestInfo));
                await this.userManager.SetLockoutEndDateAsync(userDTO.Id, DateTimeOffset.MinValue);
            }
        }
    }

    public class ApplicationRefreshTokenProvider : AuthenticationTokenProvider
    {
        private IConfigurationService configurationService;

        public ApplicationRefreshTokenProvider(IConfigurationService configurationService)
        {
            this.configurationService = configurationService;
        }

        public override void Create(AuthenticationTokenCreateContext context)
        {
            context.Ticket.Properties.ExpiresUtc = new DateTimeOffset(DateTime.Now.AddSeconds(this.configurationService.RefreshTokenExpiryTimeInSeconds));
            context.SetToken(context.SerializeTicket());
        }

        public override void Receive(AuthenticationTokenReceiveContext context)
        {
            context.DeserializeTicket(context.Token);
        }
    }
}