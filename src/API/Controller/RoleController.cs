﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using API.Infrastructure;
using Core.Attribute;
using Core.Constant;
using Core.DTO;
using Core.Entity;
using Core.Enum;
using Core.IService;
using Recipe.Core.Base.Generic;

namespace API.Controller
{
    [CustomAuthorize]
    [CanAccess(UserPermissions.SystemUsers)]
    [RoutePrefix("Role")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class RoleController : Controller<IRoleService, RoleDTO, ApplicationRole, string>
    {
        public RoleController(IRoleService service)
            : base(service)
        {
        }

        [HttpGet]
        [Route("Get")]
        public async Task<ResponseMessageResult> GetAll()
        {
            var result = await this.Service.GetAllAsync();
            if (result != null)
            {
                return await this.JsonApiSuccessAsync(Message.SuccessResult, result);
            }

            return await this.JsonApiNoContentAsync(Message.NoContent, null);
        }

        [HttpGet]
        [Route("GetRoleMapping")]
        public async Task<ResponseMessageResult> GetRoleMapping()
        {
            var result = this.Service.GetCurrentUserRoleMapping();
            if (result != null)
            {
                return await this.JsonApiSuccessAsync(Message.SuccessResult, result);
            }

            return await this.JsonApiNoContentAsync(Message.NoContent, null);
        }
    }
}
