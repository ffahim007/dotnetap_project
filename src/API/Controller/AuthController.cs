﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using API.Infrastructure;
using Common.Attribute;
using Core.Attribute;
using Core.Constant;
using Core.DTO;
using Core.Enum;
using Core.Infrastructure;
using Core.IService;

namespace API.Controller
{
    [CustomAuthorize]
    [CanAccess(UserPermissions.SystemUsers)]
    [RoutePrefix("Auth")]
    public class AuthController : Recipe.Core.Base.Generic.Controller
    {
        private IUserService userService;
        private IAuthService authService;
        private IConfigurationService configurationService;
        private IExceptionHelper exceptionHelper;

        public AuthController(IUserService userService, IConfigurationService configurationService, IExceptionHelper exceptionHelper, IAuthService authService)
        {
            this.userService = userService;
            this.authService = authService;
            this.configurationService = configurationService;
            this.exceptionHelper = exceptionHelper;
        }

        [Route("Login")]
        [HttpPost]
        [AllowAnonymous]
        [AuthorizeAnonymousApi]
        public async Task<ResponseMessageResult> Login(AuthDTO auth)
        {
            auth.Role = null;
            var result = await this.authService.LoginUserAsync(auth);
            return await this.JsonApiSuccessAsync(Message.Successful, result);
        }

        [Route("SocialLoginAT")]
        [HttpPost]
        [AllowAnonymous]
        [AuthorizeAnonymousApi]
        public async Task<ResponseMessageResult> SocialLoginAT(string accessToken)
        {
            var result = await this.userService.GetFacebookAccountAsync(accessToken);
            return await this.JsonApiSuccessAsync(Message.Successful, result);
        }

        [Route("SocialLogin")]
        [HttpPost]
        [AllowAnonymous]
        [AuthorizeAnonymousApi]
        public async Task<ResponseMessageResult> SocialLogin(UserDTO dtoObject)
        {
            var user = await this.userService.FindByClientId(dtoObject.ClientId);

            AuthDTO auth = new AuthDTO();
            auth.Password = dtoObject.Password;
            auth.EmailId = dtoObject.Email;
            auth.DeviceToken = dtoObject.DeviceToken;
            auth.DeviceType = dtoObject.DeviceType;

            if (user != null)
            {
                var result = await this.authService.LoginUserAsync(auth);
                if (result != null)
                {
                    return await this.JsonApiSuccessAsync(Message.SuccessResult, result);
                }

                return await this.JsonApiNoContentAsync(Message.NoContent, null);
            }
            else
            {
                var userDTO = await this.userService.CreateAsync(dtoObject);

                var result = await this.authService.LoginUserAsync(auth);

                if (result != null)
                {
                    return await this.JsonApiSuccessAsync(Message.SuccessResult, result);
                }

                return await this.JsonApiNoContentAsync(Message.NoContent, null);
            }
        }

        [Route("RefreshToken")]
        [HttpPost]
        [AllowAnonymous]
        [AuthorizeAnonymousApi]
        public async Task<ResponseMessageResult> RefreshToken(RefreshTokenDTO refreshTokenData)
        {
            var result = await this.authService.RefreshTokenAsync(refreshTokenData);
            return await this.JsonApiSuccessAsync(Message.Successful, result);
        }

        [Route("Logout")]
        [HttpPost]
        public async Task<ResponseMessageResult> Logout()
        {
            var result = await this.authService.LogoutUserAsync();
            return await this.JsonApiSuccessAsync(Message.Successful, result);
        }

        [Route("UpdateDeviceToken")]
        [HttpPost]
        public async Task<ResponseMessageResult> UpdateDeviceToken(UpdateDeviceTokenDTO dtoObject)
        {
            await this.authService.UpdateDeviceToken(dtoObject);
            return await this.JsonApiSuccessAsync(Message.Successful, null);
        }

        [HttpPut]
        [Route("ForgotPassword")]
        [AllowAnonymous]
        [AuthorizeAnonymousApi]
        public async Task<ResponseMessageResult> ForgetPassword(ResetPasswordLinkDTO dtoObject)
        {
            var result = await this.userService.ForgotPassword(dtoObject.UserName);
            if (!string.IsNullOrEmpty(result))
            {
                return await this.JsonApiSuccessAsync(Message.SuccessResult, new SingleValueDTO { Value = result });
            }

            return await this.JsonApiNoContentAsync(Message.NoContent, null);
        }

        [HttpPut]
        [Route("ForgotPassword/{token}")]
        [AllowAnonymous]
        [AuthorizeAnonymousApi]
        public async Task<ResponseMessageResult> ForgotPassword(string token, ForgotPasswordDTO dtoObject)
        {
            var result = await this.userService.ForgotPassword(token, dtoObject);
            if (!string.IsNullOrEmpty(result))
            {
                return await this.JsonApiSuccessAsync(Message.SuccessResult, new SingleValueDTO { Value = result });
            }

            return await this.JsonApiNoContentAsync(Message.NoContent, null);
        }

        [HttpPut]
        [Route("ValidateToken/{token}")]
        public async Task<ResponseMessageResult> ValidateToken(string token)
        {
            var result = await this.userService.ValidateTokenAsync(token);
            if (result)
            {
                return await this.JsonApiSuccessAsync(Message.SuccessResult, new SingleValueDTO { Value = result });
            }

            return await this.JsonApiBadRequestAsync(Message.UserInvalidToken, new SingleValueDTO { Value = result });
        }

        [HttpGet]
        public HttpResponseMessage Angular()
        {
            try
            {
                var response = new HttpResponseMessage();
                response.Content = new StringContent(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/index.html")));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                return response;
            }
            catch
            {
                return null;
            }
        }
    }
}
