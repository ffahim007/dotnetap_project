﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using API.Infrastructure;
using Common.Helper;
using Core.Attribute;
using Core.Constant;
using Core.DTO;
using Core.Entity;
using Core.Enum;
using Core.IService;
using Recipe.Core.Base.Generic;

namespace API.Controller
{
    [CustomAuthorize]
    [CanAccess(UserPermissions.SystemUsers)]
    [RoutePrefix("Common")]
    public class CommonController : Recipe.Core.Base.Generic.Controller
    {
    }
}
