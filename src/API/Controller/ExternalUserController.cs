﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using API.Infrastructure;
using Core.Attribute;
using Core.Constant;
using Core.DTO;
using Core.Entity;
using Core.Enum;
using Core.IService;
using Recipe.Common.Helper;
using Recipe.Core.Base.Generic;

namespace API.Controller
{
    [CustomAuthorize]
    [CanAccess(UserPermissions.ExternalUsers)]
    [RoutePrefix("ExternalUser")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ExternalUserController : ApiController
    {
        public ExternalUserController()
        {
        }
    }
}