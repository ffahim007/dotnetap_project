using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Results;
using API.Infrastructure;
using Common.Attribute;
using Core.Attribute;
using Core.Constant;
using Core.DTO;
using Core.Entity;
using Core.Enum;
using Core.IService;
using Newtonsoft.Json;
using Recipe.Common.Helper;
using Recipe.Core.Base.Generic;

namespace API.Controller
{
    [CustomAuthorize]
    [CanAccess(UserPermissions.SystemUsers)]
    [RoutePrefix("User")]
    public class UserController : Controller<IUserService, UserDTO, ApplicationUser, string>
    {
        private IAuthService authService;

        public UserController(IUserService service, IAuthService authService)
            : base(service)
        {
            this.authService = authService;
        }

        [HttpGet]
        [Route("GetSingle")]
        public async Task<ResponseMessageResult> GetSingleUser(string id)
        {
            var result = await this.Get(id);
            if (result != null)
            {
                return await this.JsonApiSuccessAsync(Message.SuccessResult, result);
            }

            return await this.JsonApiNoContentAsync(Message.NoContent, null);
        }

        [HttpPost]
        [Route("Create")]
        [AllowAnonymous]
        [AuthorizeAnonymousApi]
        public async Task<ResponseMessageResult> CreateUser(UserDTO dtoObject)
        {
            var user = await this.Service.CreateAsync(dtoObject);

            AuthDTO auth = new AuthDTO();
            auth.Password = user.Password;
            auth.EmailId = user.Email;
            auth.DeviceToken = dtoObject.DeviceToken;
            auth.DeviceType = dtoObject.DeviceType;

            var result = await this.authService.LoginUserAsync(auth);

            if (result != null)
            {
                return await this.JsonApiSuccessAsync(Message.SuccessResult, result);
            }

            return await this.JsonApiNoContentAsync(Message.NoContent, null);
        }

        [HttpPut]
        [Route("EditProfile")]
        public async Task<ResponseMessageResult> EditProfile(UserDTO dtoObject)
        {
            var result = await this.Service.UpdateAsync(dtoObject);
            if (result != null)
            {
                return await this.JsonApiSuccessAsync(Message.SuccessResult, result);
            }

            return await this.JsonApiNoContentAsync(Message.NoContent, null);
        }

        [HttpPost]
        [Route("ChangePassword")]
        public async Task<ResponseMessageResult> ChangeUserPassword(ChangePasswordDTO dtoObject)
        {
            var result = await Service.ChangePasswordAsync(dtoObject);
            if (result != null)
            {
                return await this.JsonApiSuccessAsync(Message.UserPasswordChangeSuccessfully, result);
            }

            return await this.JsonApiNoContentAsync(Message.NoContent, null);
        }

        [HttpPut]
        [Route("UpdateStatus")]
        public async Task<ResponseMessageResult> UpdateStatus(List<ChangeUserStatusDTO> dtoStatus)
        {
            await this.Service.UpdateUserStatusAsync(dtoStatus);
            return await this.JsonApiSuccessAsync(Message.SuccessResult, null);
        }

        [HttpPost]
        [Route("UnlockUser")]
        public async Task<ResponseMessageResult> UnlockUser(UserIdDTO userId)
        {
            var result = await this.Service.UnlockUserAsync(userId.Id);
            if (result)
            {
                return await this.JsonApiSuccessAsync(Message.UserUnlockSuccessful, new SingleValueDTO { Value = result });
            }

            return await this.JsonApiBadRequestAsync(Message.Error, null);
        }

        [Route("UploadImage")]
        [HttpPost]
        [AllowAnonymous]
        [AuthorizeAnonymousApi]
        public async Task<ResponseMessageResult> UploadImage()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                ////return this.StatusCode(HttpStatusCode.UnsupportedMediaType);
                return await this.JsonApiNoContentAsync(Message.NoContent, "Unsupported Media Type");
            }

            var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();
            var fileBytes = await filesReadToProvider.Contents[0].ReadAsByteArrayAsync();

            var ext = Path.GetExtension(filesReadToProvider.Contents[0].Headers.ContentDisposition.FileName.Replace("\"", string.Empty).Replace(@"\", string.Empty));
            var filename = DateTime.UtcNow.ToString().Replace("/", string.Empty).Replace(":", string.Empty).Replace(" ", string.Empty) + ext;
            string filePath = "/Images/" + filename;
            File.WriteAllBytes(HostingEnvironment.MapPath(filePath), fileBytes);

            var result = filePath;

            if (result != null)
            {
                return await this.JsonApiSuccessAsync(Message.SuccessResult, result);
            }

            return await this.JsonApiNoContentAsync(Message.NoContent, null);

            //We will use two content part one is used to store the json another is used to store the image binary.
            //var json = await filesReadToProvider.Contents[0].ReadAsStringAsync();
            //CommonDTO obj = JsonConvert.DeserializeObject<CommonDTO>(json);
        }

        [Route("UploadMultiImage")]
        [HttpPost]
        [AllowAnonymous]
        [AuthorizeAnonymousApi]
        public async Task<ResponseMessageResult> UploadMultiImage()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                ////return this.StatusCode(HttpStatusCode.UnsupportedMediaType);
                return await this.JsonApiNoContentAsync(Message.NoContent, "Unsupported Media Type");
            }

            var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();
            var result = await Service.MultiImageUploadAsync(filesReadToProvider);

            if (result != null)
            {
                return await this.JsonApiSuccessAsync(Message.SuccessResult, result);
            }

            return await this.JsonApiNoContentAsync(Message.NoContent, null);
        }

        [Route("Upload")]
        [HttpPost]
        public void Upload()
        {
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                for (int i = 0; i < HttpContext.Current.Request.Files.Count; i++)
                {
                    var httpPostedFile = HttpContext.Current.Request.Files[i];

                    if (httpPostedFile != null)
                    {
                        // Validate the uploaded image(optional)

                        // Get the complete file path
                        var fileSavePath = Path.Combine(HostingEnvironment.MapPath("~\\Images\\"), httpPostedFile.FileName);

                        // Save the uploaded file to "UploadedFiles" folder
                        httpPostedFile.SaveAs(fileSavePath);
                    }
                }
            }
        }
    }
}