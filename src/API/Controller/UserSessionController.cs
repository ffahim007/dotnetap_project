﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Core.Attribute;
using Core.DTO;
using Core.Entity;
using Core.Enum;
using Core.IService;
using Recipe.Core.Base.Generic;

namespace API.Controller
{
    [CustomAuthorize]
    [CanAccess(UserPermissions.SystemUsers)]
    [RoutePrefix("UserSession")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class UserSessionController : Controller<IUserSessionService, UserSessionDTO, UserSession, int>
    {
        public UserSessionController(IUserSessionService service)
            : base(service)
        {
        }
    }
}
