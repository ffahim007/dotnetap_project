﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Core.Infrastructure;
using Hangfire;
using Service;

namespace API.Infrastructure
{
    public class HangfireIntegeration : IHangfireIntegeration
    {
        public Task MarkJobsExpired()
        {
            return Task.FromResult(0);
        }

        public Task UpdateAverageAndTotal()
        {
            return Task.FromResult(0);
        }

        public Task UpdateRating(int deliveryPartnerId)
        {
            return Task.FromResult(0);
        }

        public Task ForceEndOfDayBasedOnCountry(string timeZone, string country, int countryId, string timeDifference)
        {
            if (string.IsNullOrEmpty(timeZone))
            {
                var splitTimeDifference = timeDifference.Trim().Split(':');
                int hours = 23 + int.Parse(splitTimeDifference[0]);
                int mins = hours < 23 ? 60 - int.Parse(splitTimeDifference[1]) + 30 : int.Parse(splitTimeDifference[1]);
                hours = hours > 24 ? hours - 24 : hours;
                var executionTime = mins + " " + hours + " * * *";
                return Task.FromResult(0);
            }

            return Task.FromResult(0);
        }

        public Task CreateFutureJobBasedOnCountry(string timeZone, string country, int countryId, string timeDifference)
        {
            if (string.IsNullOrEmpty(timeZone))
            {
                var splitTimeDifference = timeDifference.Trim().Split(':');
                int hours = 8 + int.Parse(splitTimeDifference[0]);
                int mins = hours < 8 ? 60 - int.Parse(splitTimeDifference[1]) : int.Parse(splitTimeDifference[1]);
                hours = hours < 0 ? hours + 24 : hours;
                var executionTime = mins + " " + hours + " * * *";
                return Task.FromResult(0);
            }

            return Task.FromResult(0);
        }

        public Task NotifyForDocumentExpiryBasedOnCountry(int deliveryPartnerId, string timeZone, string country, int countryId, string timeDifference)
        {
            if (string.IsNullOrEmpty(timeZone))
            {
                var splitTimeDifference = timeDifference.Trim().Split(':');
                int hours = 18 + int.Parse(splitTimeDifference[0]);
                int mins = hours < 18 ? 60 - int.Parse(splitTimeDifference[1]) : int.Parse(splitTimeDifference[1]);
                hours = hours < 0 ? hours + 24 : hours;
                var executionTime = mins + " " + hours + " * * *";
                return Task.FromResult(0);
            }

            return Task.FromResult(0);
        }
    }
}