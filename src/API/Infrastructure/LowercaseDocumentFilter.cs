﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Swashbuckle.Swagger;

namespace API.Infrastructure
{
    public class LowercaseDocumentFilter : IDocumentFilter, IOperationFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry, IApiExplorer apiExplorer)
        {
            var paths = swaggerDoc.paths;

            //generate the new keys
            var newPaths = new Dictionary<string, PathItem>();
            var removeKeys = new List<string>();
            foreach (var path in paths)
            {
                var newKey = path.Key.ToLower();
                if (newKey != path.Key)
                {
                    removeKeys.Add(path.Key);
                    newPaths.Add(newKey, path.Value);
                }
            }

            //add the new keys
            foreach (var path in newPaths)
            {
                swaggerDoc.paths.Add(path.Key, path.Value);
            }

            //remove the old keys
            foreach (var key in removeKeys)
            {
                swaggerDoc.paths.Remove(key);
            }
        }

        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation == null)
            {
                return;
            }

            if (operation.parameters == null)
            {
                operation.parameters = new List<Parameter>();
            }

            if (apiDescription.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any())
            {
                var parameter = new Parameter
                {
                    description = "Anonymous Token",
                    @in = "header",
                    name = "token",
                    required = true,
                    type = "string",
                    @default = "b2e684d7-8807-4232-b5fc-1a6e80c175c0"
                };

                parameter.required = false;
                operation.parameters.Add(parameter);
            }
            else
            {
                var parameter = new Parameter
                {
                    description = "Anonymous Token",
                    @in = "header",
                    name = "Authorization",
                    required = true,
                    type = "string",
                    @default = "bearer "
                };

                parameter.required = false;
                operation.parameters.Add(parameter);
            }
        }
    }
}