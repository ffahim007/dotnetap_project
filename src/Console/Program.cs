﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using FinanceHouse.Common.Helper;
using FinanceHouse.Core;
using FinanceHouse.Core.Constant;
using FinanceHouse.Core.DTO;
using FinanceHouse.Core.Enum;
using FinanceHouse.Core.IService;
using Recipe.Common.Helper;

namespace FinanceHouse.Console
{
    public class Program
    {
        private IUserService userService;
        
        public void SeedOldData()
        {
            var result = this.GetDataFromDB();
        }

        private static void Main(string[] args)
        {
            // Service.Thread.EmailNotificationThread thread = new Service.Thread.EmailNotificationThread();
            // thread.Execute(string.Empty);
            
            //Program prog = new Program();
            //prog.SeedOldData();
        }  

        private void ResolveIoC()
        {
            this.userService = IoC.Resolve<IUserService>();
        }

        private List<OldDataDTO> GetDataFromDB()
        {
            IConfigurationService configService = IoC.Resolve<IConfigurationService>();
            string connectionString = configService.DataMigrationConnectionString;
            string queryString =
                @"SELECT 
                    REGION_CODE,REGION,RSM_ID,RSM_NAME,
                    AREA_CODE,AREA,AM_NAME,AM_ID,
                    TERRITORY_CODE,TERRITORY,TM_CODE, TM_ID, TM_NAME,
                    DIST_CODE,DIST_NAME,TOWN_CODE,TOWN,DSR_CODE,DSR_NAME,
                    [Area Type ] AS Area_Type, [NIC (enter with '-')] AS NIC,
					Account, [Market Type ] AS Market_Type, [Pop Type] AS Pop_Type
                FROM userData";
            DataTable table = new DataTable();
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                try
                {
                    connection.Open();
                    SqlDataAdapter da = null;
                    using (da = new SqlDataAdapter(command))
                    {
                        da.Fill(table);
                    }
                }
                catch
                {
                }
            }

            var list = new List<OldDataDTO>();
            foreach (DataRow row in table.Rows)
            {
                OldDataDTO dtoObject = new OldDataDTO
                {
                    Account = row["Account"].ToString(),
                    AM_ID = row["AM_ID"].ToString(),
                    AM_NAME = row["AM_NAME"].ToString(),
                    AREA = row["AREA"].ToString(),
                    AREA_CODE = row["AREA_CODE"].ToString(),
                    Area_Type = row["Area_Type"].ToString(),
                    DIST_CODE = row["DIST_CODE"].ToString(),
                    DIST_NAME = row["DIST_NAME"].ToString(),
                    DSR_CODE = row["DSR_CODE"].ToString(),
                    DSR_NAME = row["DSR_NAME"].ToString(),
                    Market_Type = row["Market_Type"].ToString(),
                    NIC = row["NIC"].ToString(),
                    Pop_Type = row["Pop_Type"].ToString(),
                    REGION = row["REGION"].ToString(),
                    REGION_CODE = row["REGION_CODE"].ToString(),
                    RSM_ID = row["RSM_ID"].ToString(),
                    RSM_NAME = row["RSM_NAME"].ToString(),
                    TERRITORY = row["TERRITORY"].ToString(),
                    TERRITORY_CODE = row["TERRITORY_CODE"].ToString(),
                    TM_CODE = row["TM_CODE"].ToString(),
                    TM_ID = row["TM_ID"].ToString(),
                    TM_NAME = row["TM_NAME"].ToString(),
                    TOWN = row["TOWN"].ToString(),
                    TOWN_CODE = row["TOWN_CODE"].ToString()
                };
                list.Add(dtoObject);
            }

            return list;
        }
    }
}
