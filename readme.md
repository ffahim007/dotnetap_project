# Steps to setup API project

1. Clone repository into folder
2. Open and build solution
3. Clone FileGenerator repository "git clone https://ffahim007@bitbucket.org/ffahim007/filegenerator.git"
4. Switch branch to newStructure, then open and run FileGenerator project
5. Put path of api folder "YOURPATH\dotnetap_project\src" input api path field without quotation marks
6. Put json into entity textare and click create button to generate classess
7. Install extension "Format All Files" created by munyabe into Visual Studio
8. Restart Visual Studio
9. Open API project and click on Solution Explorer
10. Right click on Solution and click Format All Files
